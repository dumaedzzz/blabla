import warehousesSlice from "../slices/warehousesSlice";

export const {
	fetchWarehousesRequest,
	fetchWarehousesSuccess,
	fetchWarehousesFailure,
  createWarehouseRequest,
  createWarehouseSuccess,
  createWarehouseFailure,
  editWarehouseRequest,
  editWarehouseSuccess,
  editWarehouseFailure,
  fetchWarehouseRequest,
  fetchWarehouseSuccess,
  fetchWarehouseFailure,
  deactivateWarehouseRequest,
  deactivateWarehouseSuccess,
  deactivateWarehouseFailure
} = warehousesSlice.actions;