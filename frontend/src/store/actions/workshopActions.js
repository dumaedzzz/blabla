import workshopsSlice from "../slices/workshopsSlice";

export const {
	fetchWorkshopsRequest,
	fetchWorkshopsSuccess,
	fetchWorkshopsFailure,
} = workshopsSlice.actions;