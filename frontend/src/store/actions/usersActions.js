import usersSlice from "../slices/usersSlice";

export const {
  registerUser,
  registerUserSuccess,
  registerUserFailure,
  logInUser,
  logInUserSuccess,
  logInUserFailure,
  logOutUser,
  logOutUserSuccess,
  logOutUserFailure,
  logInEmployee,
  logInEmployeeSuccess,
  logInEmployeeFailure,
  logOutEmployee,
  logOutEmployeeSuccess,
  logOutEmployeeFailure,
} = usersSlice.actions;