import ordersSlice from "../slices/ordersSlice";

export const {
  fetchOrdersSuccess,
  fetchOrdersRequest,
  fetchOrdersFailure,
  createOrderRequest,
  createOrderFailure,
  createOrderSuccess,
  changeOrderSuccess,
  changeOrderFailure,
  changeOrderRequest,
  changeStatusSuccess,
  changeStatusFailure,
  changeStatusRequest,
  removeOrderSuccess,
  removeOrderFailure,
  removeOrderRequest
} = ordersSlice.actions;