import typeDetailsSlice from "../slices/typeDetailsSlice";

export const {
	fetchTypesDetailsRequest,
	fetchTypesDetailsSuccess,
	fetchTypesDetailsFailure,
} = typeDetailsSlice.actions;