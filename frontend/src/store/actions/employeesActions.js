import employeeSlice from "../slices/employeesSlice";

export const {
	createEmployee,
	createEmployeeSuccess,
	createEmployeeFailure,
	fetchEmployees,
	fetchEmployeesSuccess,
	fetchEmployeesFailure,
	fetchEmployee,
	fetchEmployeeSuccess,
	fetchEmployeeFailure,
	editEmployee,
	editEmployeeSuccess,
	editEmployeeFailure,
} = employeeSlice.actions;