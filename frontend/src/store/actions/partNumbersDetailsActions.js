import partNumberDetailsSlice from "../slices/partNumbersDetailsSlice";

export const {
	fetchPartNumbersDetailsRequest,
	fetchPartNumbersDetailsSuccess,
	fetchPartNumbersDetailsFailure,
} = partNumberDetailsSlice.actions;