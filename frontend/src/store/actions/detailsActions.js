import detailsSlice from "../slices/detailsSlice";

export const {
	addDetailRequest,
	addDetailSuccess,
	addDetailFailure,
} = detailsSlice.actions;