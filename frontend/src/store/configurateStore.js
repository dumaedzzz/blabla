import {combineReducers} from "redux";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import createSagaMiddleware from 'redux-saga';
import {configureStore} from "@reduxjs/toolkit";
import {rootSagas} from "./rootSagas";
import usersSlice from "./slices/usersSlice";
import employeesSlice from "./slices/employeesSlice";
import warehousesSlice from "./slices/warehousesSlice";
import workshopsSlice from "./slices/workshopsSlice";
import newsSlice from "./slices/newsSlice";
import typeDetailsSlice from "./slices/typeDetailsSlice";
import partNumbersDetailsSlice from "./slices/partNumbersDetailsSlice";
import detailsSlice from "./slices/detailsSlice";
import ordersSlice from "./slices/ordersSlice";

const rootReducer = combineReducers({
  users: usersSlice.reducer,
  employees: employeesSlice.reducer,
  warehouses: warehousesSlice.reducer,
  workshops: workshopsSlice.reducer,
  news: newsSlice.reducer,
  typesDetails: typeDetailsSlice.reducer,
  partNumbersDetails: partNumbersDetailsSlice.reducer,
  details: detailsSlice.reducer,
  orders: ordersSlice.reducer,
});
const persistedState = loadFromLocalStorage();

const sagaMiddleware = createSagaMiddleware();

const middleware = [
  sagaMiddleware,
];

const store = configureStore({
  reducer: rootReducer,
  middleware,
  devTools: true,
  preloadedState: persistedState,
});

store.subscribe(() => {
  saveToLocalStorage({
    users: store.getState().users ? {...store.getState().users, profile: store.getState().users?.profile} : null,
  });
})

sagaMiddleware.run(rootSagas);

export default store;