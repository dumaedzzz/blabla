import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {
  changeNewsFailure,
  changeNewsRequest,
  changeNewsSuccess,
  createNewsFailure,
  createNewsRequest,
  createNewsSuccess,
  fetchNewsFailure,
  fetchNewsRequest,
  fetchNewsSuccess,
  removeNewsFailure,
  removeNewsRequest,
  removeNewsSuccess
} from "../actions/newsActions";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";

export function* fetchNewsSaga() {
  try {
    const {data} = yield axiosApi.get('/news');
    yield put(fetchNewsSuccess(data));
  } catch (error) {
    yield put(fetchNewsFailure(error.response.data));
  }
}

export function* createNewsSaga({payload: latestNews}) {
  try {
    const {data} = yield axiosApi.post('/news', latestNews);
    yield put(createNewsSuccess(data));
    yield put(historyPush('/'));
    toast.success('Успешно создано.')
  } catch (error) {
    toast.error(error?.response?.data?.message);
    yield put(createNewsFailure(error.response.data));
  }
}

export function* changeNewsSaga({payload}) {
  try {
    const {data} = yield axiosApi.put('/news/' + payload.id, payload.data);
    yield put(changeNewsSuccess(data));
    yield put(historyPush('/'));
    toast.success('Успешно изменено.')
  } catch (error) {
    toast.error(error?.response?.data?.message);
    yield put(changeNewsFailure(error.response.data));
  }
}

export function* removeNewsSaga({payload: id}) {
  try {
    yield axiosApi.delete('/news/' + id);
    yield put(removeNewsSuccess(id));
    yield put(historyPush('/'));
    toast.success('Успешно удалено.')
  } catch (error) {
    toast.error(error?.response?.data?.message);
    yield put(removeNewsFailure(error.response.data));
  }
}

const newsSaga = [
  takeEvery(fetchNewsRequest, fetchNewsSaga),
  takeEvery(createNewsRequest, createNewsSaga),
  takeEvery(changeNewsRequest, changeNewsSaga),
  takeEvery(removeNewsRequest, removeNewsSaga),
];

export default newsSaga;