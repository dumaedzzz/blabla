import {put, takeEvery} from "redux-saga/effects";
import {
  logInEmployee, logInEmployeeFailure, logInEmployeeSuccess,
  logInUser,
  logInUserFailure,
  logInUserSuccess,
  logOutEmployee, logOutEmployeeFailure, logOutEmployeeSuccess,
  logOutUser,
  logOutUserFailure,
  logOutUserSuccess,
  registerUser,
  registerUserFailure,
  registerUserSuccess
} from "../actions/usersActions";
import {historyReplace} from "../actions/historyActions";
import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {EMPLOYEES_LOG_IN_URL, USERS_LOG_IN_URL, USERS_URL} from "../../config";

export function* registerUserSaga({payload: userData}) {
  try {
    yield axiosApi.post(USERS_URL, userData);
    yield put(registerUserSuccess());
    yield put(historyReplace('/'));
    toast.warning('Регистрация принята. С вами свяжется администратор для потверждения регистрации');
  } catch (err) {
    if (!err.response) toast.error(err.message);
    yield put(registerUserFailure(err.response?.data));
  }
}

export function* logInUserSaga({payload: userData}) {
  try {
    const {data} = yield axiosApi.post(USERS_LOG_IN_URL, userData);
    yield put(logInUserSuccess(data));
    yield put(historyReplace('/'));
  } catch (err) {
    if (!err.response) toast.error(err.message);
    if (err.response?.data?.global) toast.error(err.response?.data?.global);
    yield put(logInUserFailure(err.response?.data));
  }
}

export function* logOutUserSaga({payload: userData}) {
  try {
    yield axiosApi.delete(USERS_LOG_IN_URL, userData);
    yield put(logOutUserSuccess());
    yield put(historyReplace('/'));
    toast.success('Вы успешно вышли из системы');
  } catch (err) {
    if (!err.response) toast.error(err.message);
    if (err.response?.data?.global) toast.error(err.response?.data?.global);
    yield put(logOutUserFailure(err.response?.data));
  }
}

export function* logInEmployeeSaga({payload: userData}) {
  try {
    const {data} = yield axiosApi.post(EMPLOYEES_LOG_IN_URL, userData);
    yield put(logInEmployeeSuccess(data));
    yield put(historyReplace('/'));
  } catch (err) {
    if (!err.response) toast.error(err.message);
    if (err.response?.data?.global) toast.error(err.response?.data?.global);
    yield put(logInEmployeeFailure(err.response?.data));
  }
}

export function* logOutEmployeeSaga({payload: userData}) {
  try {
    yield axiosApi.delete(EMPLOYEES_LOG_IN_URL, userData);
    yield put(logOutEmployeeSuccess());
    yield put(historyReplace('/'));
    toast.success('Вы успешно вышли из системы');
  } catch (err) {
    if (!err.response) toast.error(err.message);
    yield put(logOutEmployeeFailure(err.response?.data));
  }
}

const usersSagas = [
  takeEvery(registerUser, registerUserSaga),
  takeEvery(logInUser, logInUserSaga),
  takeEvery(logOutUser, logOutUserSaga),
  takeEvery(logInEmployee, logInEmployeeSaga),
  takeEvery(logOutEmployee, logOutEmployeeSaga),
];

export default usersSagas;