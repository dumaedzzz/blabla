import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {toast} from "react-toastify";
import {
	fetchPartNumbersDetailsFailure,
	fetchPartNumbersDetailsRequest,
	fetchPartNumbersDetailsSuccess
} from "../actions/partNumbersDetailsActions";

export function* fetchPartNumbersDetailsSaga() {
	try {
		const response = yield axiosApi.get('/partNumbersDetails');
		yield put(fetchPartNumbersDetailsSuccess(response.data));
	} catch (error) {
		yield put(fetchPartNumbersDetailsFailure());
		toast.error(error);
	}
}

const partNumbersDetailSaga = [
	takeEvery(fetchPartNumbersDetailsRequest, fetchPartNumbersDetailsSaga),
];

export default partNumbersDetailSaga;
