import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {toast} from "react-toastify";
import {
	fetchTypesDetailsFailure,
	fetchTypesDetailsRequest,
	fetchTypesDetailsSuccess
} from "../actions/typesDetailsActions";

export function* fetchTypesDetailsSagas() {
	try {
		const response = yield axiosApi.get('/typeDetails');
		yield put(fetchTypesDetailsSuccess(response.data));
	} catch (error) {
		yield put(fetchTypesDetailsFailure());
		toast.error(error);
	}
}

const typesDetailsSaga = [
	takeEvery(fetchTypesDetailsRequest, fetchTypesDetailsSagas),
];

export default typesDetailsSaga;