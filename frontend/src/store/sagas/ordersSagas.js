import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {
  fetchOrdersSuccess,
  fetchOrdersRequest,
  fetchOrdersFailure,
  createOrderRequest,
  createOrderFailure,
  createOrderSuccess,
  changeOrderSuccess,
  changeOrderFailure,
  changeOrderRequest,
  changeStatusSuccess,
  changeStatusFailure,
  changeStatusRequest,
  removeOrderSuccess,
  removeOrderFailure,
  removeOrderRequest
} from "../actions/ordersActions";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";

export function* fetchOrdersSaga() {
  try {
    const {data} = yield axiosApi.get('/orders');
    yield put(fetchOrdersSuccess(data));
  } catch (error) {
    yield put(fetchOrdersFailure(error?.response?.data));
  }
}

export function* createOrderSaga({payload: order}) {
  try {
    const {data} = yield axiosApi.post('/orders', order);
    yield put(createOrderSuccess(data));
    yield put(historyPush('/orderPage'));
    toast.success('Успешно создано.')
  } catch (error) {
    toast.error(error?.response?.data?.message);
    yield put(createOrderFailure(error.response.data));
  }
}

export function* changeOrderSaga({payload}) {
  try {
    const {data} = yield axiosApi.put('/orders/' + payload.id, payload.data);
    yield put(changeOrderSuccess(data));
    yield put(historyPush('/orderPage'));
    toast.success('Успешно изменено.')
  } catch (error) {
    toast.error(error?.response?.data?.message);
    yield put(changeOrderFailure(error.response.data));
  }
}

export function* removeOrderSaga({payload: id}) {
  try {
    yield axiosApi.delete('/orders/' + id);
    yield put(removeOrderSuccess(id));
    yield put(historyPush('/'));
    toast.success('Успешно удалено.')
  } catch (error) {
    toast.error(error?.response?.data?.message);
    yield put(removeOrderFailure(error.response.data));
  }
}

export function* changeStatusSaga({payload: id}) {
  try {
    const {data} = yield axiosApi.put('/news/' + id);
    yield put(changeStatusSuccess(data));
    yield put(historyPush('/orderPage'));
    toast.success('Успешно удалено.')
  } catch (error) {
    toast.error(error?.response?.data?.message);
    yield put(changeStatusFailure(error.response.data));
  }
}

const ordersSaga = [
  takeEvery(fetchOrdersRequest, fetchOrdersSaga),
  takeEvery(createOrderRequest, createOrderSaga),
  takeEvery(changeOrderRequest, changeOrderSaga),
  takeEvery(removeOrderRequest, removeOrderSaga),
  takeEvery(changeStatusRequest, changeStatusSaga),
];

export default ordersSaga;
