import axiosApi from "../../axiosApi";
import {put, takeEvery} from "redux-saga/effects";
import {
  createWarehouseFailure,
  createWarehouseRequest,
  createWarehouseSuccess, deactivateWarehouseFailure, deactivateWarehouseRequest, deactivateWarehouseSuccess,
  editWarehouseFailure,
  editWarehouseRequest,
  editWarehouseSuccess,
  fetchWarehouseFailure,
  fetchWarehouseRequest,
  fetchWarehousesFailure,
  fetchWarehousesRequest,
  fetchWarehousesSuccess,
  fetchWarehouseSuccess
} from "../actions/warehousesActions";
import {toast} from "react-toastify";

export function* fetchWarehousesSaga() {
	try {
		const response = yield axiosApi.get('/warehouses');
		yield put(fetchWarehousesSuccess(response.data));
	} catch (error) {
		yield put(fetchWarehousesFailure(error.response.data.error));
	}
}

export function* fetchWarehouseSaga({payload: warehouseId}) {
  try {
    const response = yield axiosApi.get(`/warehouses?warehouse=${warehouseId}`);
    yield put(fetchWarehouseSuccess(response.data));
  } catch (error) {
    yield put(fetchWarehouseFailure(error.response.data.error));
  }
}

export function* createWarehouseSagas({payload: warehouseData}) {
  try {
    yield axiosApi.post('/warehouses/new', warehouseData);
    yield put(createWarehouseSuccess());
    yield put(fetchWarehousesRequest());
    toast.success('Склад создан');
  } catch (e) {
    yield put(createWarehouseFailure(e.response.data));
    toast.error('Не получилось создать склад');
  }
}

export function* editWarehouseSagas({payload: warehouse}) {
  try {
    yield axiosApi.put('/warehouses/changeName/'+ warehouse._id, warehouse);
    yield put(editWarehouseSuccess());
    yield put(fetchWarehousesRequest());
    toast.success('Название склада измененно');
  } catch (error) {
    yield put(editWarehouseFailure(error.response.data));
    toast.error(error.response.data.error);
  }
}

export function* deactivateWarehouseSagas({payload: id}) {
  try {
    yield axiosApi.put('/warehouses/changeStatus/' + id, null);
    yield put(deactivateWarehouseSuccess());
  } catch (e) {
    yield put(deactivateWarehouseFailure(e.response.data));
    toast.error('Не получилось деактивировать!')
  }
}


const warehousesSaga = [
	takeEvery(fetchWarehousesRequest, fetchWarehousesSaga),
  takeEvery(createWarehouseRequest, createWarehouseSagas),
  takeEvery(fetchWarehouseRequest, fetchWarehouseSaga),
  takeEvery(editWarehouseRequest, editWarehouseSagas),
  takeEvery(deactivateWarehouseRequest, deactivateWarehouseSagas),
];

export default warehousesSaga;