import {put, takeEvery} from "redux-saga/effects";
import {addDetailFailure, addDetailRequest, addDetailSuccess} from "../actions/detailsActions";
import axiosApi from "../../axiosApi";
import {historyPush} from "../actions/historyActions";
import {toast} from "react-toastify";

export function* addDetailSaga({payload: detailData}) {
	try {
		yield axiosApi.post(`/details${detailData.id}`, detailData.data);
		yield put(addDetailSuccess());
		yield put(historyPush('/admin'));
		toast.success('Данные успешно сохранены!');
	} catch (error) {
		yield put(addDetailFailure(error.response.data));
		toast.error('Заполните все обязательные поля!');
	}
}

const detailsSagas = [
	takeEvery(addDetailRequest, addDetailSaga),
];

export default detailsSagas;