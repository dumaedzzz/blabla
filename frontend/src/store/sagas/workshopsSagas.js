import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {fetchWorkshopsFailure, fetchWorkshopsRequest, fetchWorkshopsSuccess} from "../actions/workshopActions";

export function* fetchWorkshopsSagas() {
	try {
		const response = yield axiosApi.get('/workshops');
		yield put(fetchWorkshopsSuccess(response.data));
	} catch (error) {
		yield put(fetchWorkshopsFailure(error.response.data.error));
	}
}

const workshopsSaga = [
	takeEvery(fetchWorkshopsRequest, fetchWorkshopsSagas),
];

export default workshopsSaga;