import {put, takeEvery} from "redux-saga/effects";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {historyPush} from "../actions/historyActions";
import {
	createEmployee,
	createEmployeeFailure,
	createEmployeeSuccess,
	editEmployee,
	editEmployeeFailure,
	editEmployeeSuccess,
	fetchEmployee,
	fetchEmployeeFailure,
	fetchEmployees,
	fetchEmployeesFailure,
	fetchEmployeesSuccess,
	fetchEmployeeSuccess,
} from "../actions/employeesActions";

export function* createEmployeesSagas({payload: employeeData}) {
	try {
		yield axiosApi.post('/employees/new', employeeData);
		yield put(createEmployeeSuccess());
		yield put(historyPush('/employee'));
		toast.success('Создан новый сотрудник!');
	} catch (error) {
		yield put(createEmployeeFailure(error.response.data));
		toast.error('У вас есть ошибка, пожалуйста проверьте!');
	}
}

export function* fetchEmployeesSagas() {
	try {
		const response = yield axiosApi.get('/employees');
		yield put(fetchEmployeesSuccess(response.data));
	} catch (error) {
		yield put(fetchEmployeesFailure(error));
	}
}

export function* fetchEmployeeSagas({payload: employeeId}) {
	try {
		const response = yield axiosApi.get('/employees' + employeeId);
		yield put(fetchEmployeeSuccess(response.data));
	} catch (error) {
		yield put(fetchEmployeeFailure(error));
		toast.error(error);
	}
}

export function* editEmployeeSagas({payload: employee}) {
	try {
		const response = yield axiosApi.put(`/employees${employee.id}`, employee.data);
		yield put(editEmployeeSuccess());
		yield put(historyPush('/employee'));
		toast.success(response.data);
	} catch (error) {
		yield put(editEmployeeFailure(error.response.data));
		toast.error(error.response.data.error);
	}
}

const employeesSaga = [
	takeEvery(createEmployee, createEmployeesSagas),
	takeEvery(fetchEmployees, fetchEmployeesSagas),
	takeEvery(editEmployee, editEmployeeSagas),
	takeEvery(fetchEmployee, fetchEmployeeSagas),
];

export default employeesSaga;