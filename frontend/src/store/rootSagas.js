import {all} from 'redux-saga/effects';
import historySagas from "./sagas/historySagas";
import usersSagas from "./sagas/usersSagas";
import createEmployeesSagas from "./sagas/employeesSagas";
import warehousesSagas from "./sagas/warehousesSagas";
import newsSagas from "./sagas/newsSagas";
import typesDetailsSaga from "./sagas/typesDetailsSagas";
import partNumbersDetailSaga from "./sagas/partNumbersDetailsSagas";
import detailsSagas from "./sagas/detailsSagas";
import ordersSagas from "./sagas/ordersSagas";
import workshopsSagas from "./sagas/workshopsSagas";

export function* rootSagas() {
  yield all([
    ...usersSagas,
    ...historySagas,
	  ...createEmployeesSagas,
    ...warehousesSagas,
    ...newsSagas,
    ...typesDetailsSaga,
    ...partNumbersDetailSaga,
    ...detailsSagas,
    ...ordersSagas,
    ...workshopsSagas,
  ]);
}