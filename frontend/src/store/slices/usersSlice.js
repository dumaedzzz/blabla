import {createSlice} from "@reduxjs/toolkit";

const name = 'users';

export const initialState = {
  profile: null,
  errSignUp: null,
  errLogIn: null,
  errLogOut: null,
  signUpLoading: false,
  logInLoading: false,
  logOutLoading: false,
};

const usersSlice = createSlice({
  name,
  initialState,
  reducers: {
    registerUser(state) {
      state.signUpLoading = true;
      state.errSignUp = null;
    },
    registerUserSuccess(state) {
      state.signUpLoading = false;
    },
    registerUserFailure(state, {payload}) {
      state.signUpLoading = false;
      state.errSignUp = payload;
    },
    logInUser(state) {
      state.logInLoading = true;
      state.errLogIn = null;
    },
    logInUserSuccess(state, {payload}) {
      state.profile = payload;
      state.logInLoading = false;
    },
    logInUserFailure(state, {payload}) {
      state.logInLoading = false;
      state.errLogIn = payload;
    },
    logOutUser(state) {
      state.logOutLoading = true;
      state.errLogOut = null;
    },
    logOutUserSuccess(state) {
      state.profile = null;
      state.logOutLoading = false;
    },
    logOutUserFailure(state, {payload}) {
      state.logOutLoading = false;
      state.errLogOut = payload;
    },
    logInEmployee(state) {
      state.logInLoading = true;
      state.errLogIn = null;
    },
    logInEmployeeSuccess(state, {payload}) {
      state.profile = payload;
      state.logInLoading = false;
    },
    logInEmployeeFailure(state, {payload}) {
      state.logInLoading = false;
      state.errLogIn = payload;
    },
    logOutEmployee(state) {
      state.logOutLoading = true;
      state.errLogOut = null;
    },
    logOutEmployeeSuccess(state) {
      state.profile = null;
      state.logOutLoading = false;
    },
    logOutEmployeeFailure(state, {payload}) {
      state.logOutLoading = false;
      state.errLogOut = payload;
    },
  }
});

export default usersSlice;