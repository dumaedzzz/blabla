import {createSlice} from "@reduxjs/toolkit";

const name = 'partNumberDetails';

const partNumberDetailsSlice = createSlice({
	name,
	initialState: {
		partNumbers: [],
		fetchPartNumbersDetailsLoading: false,
	},
	reducers: {
		fetchPartNumbersDetailsRequest(state) {
			state.fetchPartNumbersDetailsLoading = true;
		},
		fetchPartNumbersDetailsSuccess(state, action) {
			state.partNumbers = action.payload;
			state.fetchPartNumbersDetailsLoading = false;
		},
		fetchPartNumbersDetailsFailure(state) {
			state.fetchPartNumbersDetailsLoading = false;
		},
	}
});

export default partNumberDetailsSlice;