import {createSlice} from "@reduxjs/toolkit";

const name = 'typeDetails';

const typeDetailsSlice = createSlice({
	name,
	initialState: {
		types: [],
		fetchTypesDetailsLoading: false,
	},
	reducers: {
		fetchTypesDetailsRequest(state) {
			state.fetchTypesDetailsLoading = true;
		},
		fetchTypesDetailsSuccess(state, action) {
			state.types = action.payload;
			state.fetchTypesDetailsLoading = false;
		},
		fetchTypesDetailsFailure(state) {
			state.fetchTypesDetailsLoading = false;
		},
	}
});

export default typeDetailsSlice;