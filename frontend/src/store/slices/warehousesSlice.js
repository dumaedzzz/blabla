import {createSlice} from "@reduxjs/toolkit";

const name = 'warehouses';

const warehousesSlice = createSlice({
	name,
	initialState: {
		warehouses: [],
    warehouse: {},
    fetchLoading: false,
		fetchWarehousesLoading: false,
		fetchWarehousesError: null,
    fetchWarehouseLoading: false,
    fetchWarehouseError: null,
    createWarehouseLoading: false,
    createWarehouseError: null,
    editWarehouseLoading: false,
    editWarehouseError: null
	},
	reducers: {
		fetchWarehousesRequest(state) {
			state.fetchWarehousesLoading = true;
		},
		fetchWarehousesSuccess(state, action) {
			state.warehouses = action.payload;
			state.fetchWarehousesLoading = false;
			state.fetchWarehousesError = null;
		},
		fetchWarehousesFailure(state, action) {
			state.fetchWarehousesLoading = false;
			state.fetchWarehousesError = action.payload;
		},
    fetchWarehouseRequest(state) {
      state.fetchWarehouseLoading = true;
    },
    fetchWarehouseSuccess(state, action) {
      state.fetchWarehouseLoading = false;
      state.fetchWarehouseError = null;
      state.warehouse = action.payload;
    },
    fetchWarehouseFailure(state, action) {
      state.fetchWarehouseLoading = false;
      state.fetchWarehouseError = action.payload;
    },
    createWarehouseRequest(state) {
      state.createWarehouseLoading = true;
    },
    createWarehouseSuccess(state) {
      state.createWarehouseLoading = false;
      state.createWarehouseError = null;
    },
    createWarehouseFailure(state, {payload: error}) {
      state.createWarehouseLoading = false;
      state.createWarehouseError = error;
    },
    editWarehouseRequest(state) {
      state.editWarehouseLoading = true;
    },
    editWarehouseSuccess(state) {
      state.editWarehouseLoading = false;
      state.editWarehouseError = null;
    },
    editWarehouseFailure(state) {
      state.editWarehouseLoading = false;
      state.editWarehouseError = null;
    },
    deactivateWarehouseRequest(state) {
      state.fetchLoading = true;
    },
    deactivateWarehouseSuccess(state) {
      state.fetchLoading = false;
    },
    deactivateWarehouseFailure(state) {
      state.fetchLoading = false;
    }
	},
});

export default warehousesSlice;

