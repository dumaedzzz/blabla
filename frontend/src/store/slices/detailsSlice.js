const {createSlice} = require("@reduxjs/toolkit");

const name = "details";

const detailsSlice = createSlice({
	name,
	initialState: {
		addDetailLoading: false,
		addDetailError: null,
	},
	reducers: {
		addDetailRequest(state) {
			state.addDetailLoading = true;
		},
		addDetailSuccess(state) {
			state.addDetailLoading = false;
		},
		addDetailFailure(state, action) {
			state.addDetailLoading = false;
			state.addDetailError = action.payload;
		},
	},
});

export default detailsSlice;