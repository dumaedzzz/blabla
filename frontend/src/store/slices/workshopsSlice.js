import {createSlice} from "@reduxjs/toolkit";

const name = 'workshops';

const workshopsSlice = createSlice({
	name,
	initialState: {
		workshops: [],
		fetchWorkshopsLoading: false,
		fetchWorkshopsError: null,
	},
	reducers: {
		fetchWorkshopsRequest(state) {
			state.fetchWorkshopsLoading = true;
		},
		fetchWorkshopsSuccess(state, action) {
			state.workshops = action.payload;
			state.fetchWorkshopsLoading = false;
			state.fetchWorkshopsError = null;
		},
		fetchWorkshopsFailure(state, action) {
			state.fetchWorkshopsLoading = false;
			state.fetchWorkshopsError = action.payload;
		},
	}
});

export default workshopsSlice;