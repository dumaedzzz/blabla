const {makeStyles} = require("@mui/styles");

export const useStyles = makeStyles({
	formContainer: {
		marginTop: '35px',
	},
	btn: {
		display: 'block',
	},
});

