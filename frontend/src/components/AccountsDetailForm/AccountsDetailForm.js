import React, {useState} from 'react';
import {Button, CircularProgress, Grid, TextField, Typography} from "@mui/material";
import MenuItem from "@mui/material/MenuItem";
import {useStyles} from "./AccountsDetailForm.styles";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";
import AddOutlinedIcon from '@mui/icons-material/AddOutlined';

const AccountsDetailForm = (
	{typeOfAction, types, partNumbers, fetchPartNumbersLoading, fetchTypesLoading, onSubmit}) => {
	const classes = useStyles();
	const currencyOptions = ['EUR', 'USD', 'KZT', 'RUB', 'KGS'];
	const [delivery, setDelivery] = useState({currentCost: '', currency: '', rate: ''});
	const [customs, setCustoms] = useState({currentCost: '', currency: '', rate: ''});
	const [commission, setCommission] = useState({currentCost: '', currency: '', rate: ''});
	const deliveryCost = delivery.currency === 'KGS' ? delivery.currentCost : delivery.currentCost * delivery.rate;
	const customsCost = customs.currency === 'KGS' ? customs.currentCost : customs.currentCost * customs.rate;
	const commissionCost = commission.currency === 'KGS' ? commission.currentCost : commission.currentCost * commission.rate;

	const [details, setDetails] = useState([{
		type: '',
		partNumber: '',
		quantity: '',
		priceInitial: '',
		typeOfAction: typeOfAction,
	}]);

	let detailsTotal = details.reduce((acc, curr) => {
		return acc += curr.quantity * curr.priceInitial
	}, Number(deliveryCost) + Number(customsCost) + Number(commissionCost));

	const [state] = useState({
		details: '',
		deliveryPrice: 0,
		commissionPrice: 0,
		customPrice: 0,
		totalPrice: 0,
		typeOfAction: typeOfAction,
	});

	const changeDetails = (i, name, value) => {
		setDetails(prevState => {
			const detailCopy = {
				...prevState[i],
				[name]: value,
			};

			return prevState.map((detail, index) => {
				if (i === index) {
					return detailCopy;
				}

				return detail;
			});
		});
	};

	const addDetail = i => {
		const copyDetails = [...details];
		copyDetails.splice(
			i+1,
			0,
			{type: '', partNumber: '', quantity: '', priceInitial: '', priceTotal: ''}
		);
		setDetails(copyDetails);
	};

	const deleteDetail = i => {
		const newDetails = [...details];

		if (newDetails.length !== 1) {
			newDetails.splice(i, 1);
		} else {
			Object.keys(newDetails[i]).forEach(key => {
				newDetails[i][key] = ''
			});
		}

		setDetails(newDetails);
	};

	const deliveryHandler = e => {
		const {name, value} = e.target;

		setDelivery(prevState => ({
			...prevState,
			[name]: value,
		}));
	};

	const customsHandler = e => {
		const {name, value} = e.target;

		setCustoms(prevState => ({
			...prevState,
			[name]: value,
		}));
	};

	const commissionHandler = e => {
		const {name, value} = e.target;

		setCommission(prevState => ({
			...prevState,
			[name]: value,
		}));
	};

	const submitFormHandler = e => {
		e.preventDefault();

		const newState = {};

		Object.keys(state).forEach(key => {
			if (key === 'details') {
				return newState[`${key}`] = JSON.stringify(details);
			}

			if (key === 'deliveryPrice') {
				return newState[`${key}`] = deliveryCost;
			}

			if (key === 'customPrice') {
				return newState[`${key}`] = customsCost;
			}

			if (key === 'commissionPrice') {
				return newState[`${key}`] = commissionCost;
			}

			if (key === 'totalPrice') {
				return newState[`${key}`] = detailsTotal;
			}

			newState[`${key}`] = state[key];
		});

		onSubmit(newState);
	};

	return (
		<div className={classes.formContainer}>
			<Grid
				container
				component="form"
				onSubmit={submitFormHandler}
				spacing={2}
				noValidate
				mb={4}
			>
				<Grid item xs={10} mb={5}>
					{details.map((detail, i) => {
						return (
							<Grid container key={i} justifyContent="start" alignItems="center" spacing={2} mb={2}>
								{fetchTypesLoading ?
									<Grid container justifyContent="center" alignItems="center">
										<Grid item>
											<CircularProgress/>
										</Grid>
									</Grid> :
									<Grid item lg={2} md={4} sm={6} xs={12}>
										<TextField
											type="text"
											select
											required
											fullWidth
											label="Тип детали"
											name="type"
											value={details[i].type}
											onChange={e => changeDetails(i, 'type', e.target.value)}
										>
											{types.map(t => (
												<MenuItem
													key={t._id}
													value={t._id}
												>
													{t.type}
												</MenuItem>
											))}
										</TextField>
									</Grid>
								}
								{fetchPartNumbersLoading ?
									<Grid container justifyContent="center" alignItems="center">
										<Grid item>
											<CircularProgress/>
										</Grid>
									</Grid> :
									<Grid item lg={2} md={4} sm={6} xs={12}>
										<TextField
											type="text"
											select
											required
											fullWidth
											id="partNumber"
											label="Парт номер"
											name="partNumber"
											value={details[i].partNumber}
											onChange={e => changeDetails(i, 'partNumber', e.target.value)}
										>
											<MenuItem value=''>Выберите нужный партномер</MenuItem>
											{(details[i].type !== '') && partNumbers.filter(n => n.type === details[i].type).map(n => (
												<MenuItem
													key={n._id}
													value={n._id}
												>
													{n.partNumber}
												</MenuItem>
											))}
										</TextField>
									</Grid>
								}
								<Grid item lg={2} md={4} sm={6} xs={12}>
									<TextField
										type="number"
										required
										fullWidth
										label="Количество"
										name="quantity"
										value={details[i].quantity}
										onChange={e => changeDetails(i, 'quantity', e.target.value)}
									/>
								</Grid>
								<Grid item lg={2} md={4} sm={6} xs={12}>
									<TextField
										type="number"
										required
										fullWidth
										label="Цена"
										name="priceInitial"
										value={details[i].priceInitial}
										onChange={e => changeDetails(i, 'priceInitial', e.target.value)}
									/>
								</Grid>
								<Grid item lg={1} md={2} sm={3} xs={6}>
									<Button onClick={() => deleteDetail(i)}  variant="outlined" fullWidth className={classes.btn}>
										<DeleteOutlineIcon style={{fontSize: '35px', paddingTop: '6px'}}/>
									</Button>
								</Grid>
								<Grid item lg={1} md={2} sm={3} xs={6}>
									<Button onClick={() => addDetail(i)} variant="outlined" fullWidth className={classes.btn}>
										<AddOutlinedIcon style={{fontSize: '35px', paddingTop: '6px'}}/>
									</Button>
								</Grid>
							</Grid>
						)
					})}
				</Grid>
				<Grid item xs={6}>
					<Grid container justifyContent="space-between" alignItems="center" spacing={2} mb={2}>
						<Grid item lg={3} xs={12}>
							<Typography variant="h7">Доставка</Typography>
						</Grid>
						<Grid item lg={3} xs={12}>
							<TextField
								type="number"
								required
								fullWidth
								label="Цена"
								name="currentCost"
								value={delivery.currentCost}
								onChange={deliveryHandler}
							/>
						</Grid>
						<Grid item lg={3} xs={12}>
							<TextField
								type="text"
								required
								select
								fullWidth
								label="Валюта"
								name="currency"
								value={delivery.currency}
								onChange={deliveryHandler}
							>
								{currencyOptions.map((c, i) => (
									<MenuItem key={i} value={c}>
										{c}
									</MenuItem>
								))}
							</TextField>
						</Grid>
						<Grid item lg={3} xs={12}>
							<TextField
								type="number"
								fullWidth
								label="Курс валюты"
								name="rate"
								value={delivery.rate}
								onChange={deliveryHandler}
							/>
						</Grid>
					</Grid>
					<Grid container justifyContent="space-between" alignItems="center" spacing={2} mb={2}>
						<Grid item lg={3} xs={12}>
							<Typography variant="h7">Таможня</Typography>
						</Grid>
						<Grid item lg={3} xs={12}>
							<TextField
								type="number"
								required
								fullWidth
								label="Цена"
								name="currentCost"
								value={customs.currentCost}
								onChange={customsHandler}
							/>
						</Grid>
						<Grid item lg={3} xs={12}>
							<TextField
								type="text"
								required
								select
								fullWidth
								label="Валюта"
								name="currency"
								value={customs.currency}
								onChange={customsHandler}
							>
								{currencyOptions.map((c, i) => (
									<MenuItem key={i} value={c}>
										{c}
									</MenuItem>
								))}
							</TextField>
						</Grid>
						<Grid item lg={3} xs={12}>
							<TextField
								type="number"
								fullWidth
								label="Курс валюты"
								name="rate"
								value={customs.rate}
								onChange={customsHandler}
							/>
						</Grid>
					</Grid>
					<Grid container justifyContent="space-between" alignItems="center" spacing={2} mb={2}>
						<Grid item lg={3} xs={12}>
							<Typography variant="h7">Комиссия</Typography>
						</Grid>
						<Grid item lg={3} xs={12}>
							<TextField
								type="number"
								required
								fullWidth
								label="Цена"
								name="currentCost"
								value={commission.currentCost}
								onChange={commissionHandler}
							/>
						</Grid>
						<Grid item lg={3} xs={12}>
							<TextField
								type="text"
								required
								select
								fullWidth
								label="Валюта"
								name="currency"
								value={commission.currency}
								onChange={commissionHandler}
							>
								{currencyOptions.map((c, i) => (
									<MenuItem key={i} value={c}>
										{c}
									</MenuItem>
								))}
							</TextField>
						</Grid>
						<Grid item lg={3} xs={12}>
							<TextField
								type="number"
								fullWidth
								label="Курс валюты"
								name="rate"
								value={commission.rate}
								onChange={commissionHandler}
							/>
						</Grid>
						<Grid item lg={3} xs={12}>
							<Typography variant="h7">Итого: {detailsTotal} сом</Typography>
						</Grid>
					</Grid>
				</Grid>
				<Grid item xs={12}>
					<Button type="submit" variant="contained">Сохранить</Button>
				</Grid>
			</Grid>
		</div>
	)
};

export default AccountsDetailForm;