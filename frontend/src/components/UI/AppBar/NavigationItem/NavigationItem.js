import React from 'react';
import c from './NavigationItem.module.css';
import {NavLink} from "react-router-dom";

const NavigationItem = ({to, children}) => {
    return (
        <li className={c.NavigationItem}>
            <NavLink to={to}>{children}</NavLink>
        </li>
    );
};

export default NavigationItem;