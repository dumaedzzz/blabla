import React, {useState} from 'react';
import NavigationItems from "../NavigationItems/NavigationItems";
import logo from '../../../../assets/images/mainPagePictures/logo.png';
import {Link} from "react-router-dom";
import c from './Toolbar.module.css';

const Toolbar = () => {
  const [open, setOpen] = useState(false);

  return (
    <header>
      <div className={c.Toolbar}>
        <div className={`${c.header} container`}>
          <div className={`${c.header__logo}  ${open ? c.logoActive : c.logoNotActive}`}>
            <Link to={'/'}>
              <img src={logo} className={c.AppLogo} alt="logo"/>
            </Link>
            <h5 className={c.logoText}>ГДТ Бишкек</h5>
          </div>
          <nav className={c.header__links}>
            <NavigationItems/>
          </nav>
        </div>
      </div>
    </header>
  );
};

export default Toolbar;