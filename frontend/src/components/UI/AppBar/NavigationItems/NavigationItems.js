import React from 'react';
import c from './NavigationItems.module.css'
import NavigationItem from "../NavigationItem/NavigationItem";
import {makeStyles} from "@mui/styles";
import {InputBase} from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import BurgerMenu from "../BurgerMenu/BurgerMenu";
import ProfileMenu from "../../../../containers/SettingsMenu/ProfileMenu/ProfileMenu";
import {useSelector} from "react-redux";

const useStyles = makeStyles(theme => {
  return ({
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: "rgba(209, 209, 209, 0.4)",
      '&:hover': {
        backgroundColor: "rgba(209, 209, 209, 0.4)",
      },
      [theme.breakpoints.up('md')]: {
        width: 'auto',
        marginLeft: '30px',
      },
      [theme.breakpoints.up('xs')]: {
        marginBottom: "30px",
      },
      ['@media (min-width:768px)']: {
        marginBottom: "0",
      },
    },
    searchIcon: {
      top: 0,
      left: "76%",
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      borderLeft: `1px solid #1D1D1F`
    }
  });
})


const NavigationItems = () => {
  const classes = useStyles();
  const user = useSelector(state => state.users.profile);

  return (
    <>
    <div className={`${c.NavigationItems} ${c.activeBlock}`}>
      <NavigationItem to='/'>Главная</NavigationItem>
      <NavigationItem to="/news">Новости</NavigationItem>
      <NavigationItem to="/catalog">Каталог</NavigationItem>
      <div className={classes.search}>
        <InputBase
          placeholder="Введите код"
          className={c.inputSearch}
          inputProps={{'aria-label': 'search'}}
        />
        <div className={classes.searchIcon}>
          <SearchIcon style={{color: "rgba(255, 255, 255, 0.5)"}}/>
        </div>
      </div>
      {!user && (
        <NavigationItem to="/login">Войти</NavigationItem>
      )}
      {user && (
          <ProfileMenu/>
      )}
    </div>
      <div className={c.burger}>
        <BurgerMenu>
          {user && (
            <li style={{width: '100%', display: 'flex', justifyContent: 'center'}}>
              <ProfileMenu/>
            </li>
          )}
          <NavigationItem to='/'>Главная</NavigationItem>
          <NavigationItem to="/news">Новости</NavigationItem>
          <NavigationItem to="/catalog">Каталог</NavigationItem>
          <div className={classes.search}>
            <InputBase
              placeholder="Введите код"
              className={c.inputSearch}
              inputProps={{'aria-label': 'search'}}
            />
            <div className={classes.searchIcon}>
              <SearchIcon style={{color: "rgba(255, 255, 255, 0.5)"}}/>
            </div>
          </div>
          {!user && (
            <NavigationItem to="/login">Войти</NavigationItem>
          )}
        </BurgerMenu>
      </div>
    </>
  )
    ;
};

export default NavigationItems;
