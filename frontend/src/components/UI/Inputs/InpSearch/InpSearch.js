import React from 'react';
import {InputBase, Paper} from "@mui/material";
import IconButton from "@mui/material/IconButton";
import SearchIcon from '@mui/icons-material/Search';

const InpSearch = ({onChange, placeholder, fullWidth = false}) => {
  return (
    <Paper
      component="form"
      sx={{p: '2px 4px', display: 'flex', alignItems: 'center', maxWidth: 600}}
    >
      <IconButton type="submit" sx={{p: '10px'}}>
        <SearchIcon/>
      </IconButton>
      <InputBase
        onChange={onChange}
        sx={{ml: 1, flex: 1}}
        placeholder={placeholder}
        fullWidth={fullWidth}
      />
    </Paper>
  );
};

export default InpSearch;