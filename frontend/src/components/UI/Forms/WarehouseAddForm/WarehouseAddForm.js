import React, {useState} from 'react';
import {Grid, Stack, TextField, Typography} from "@mui/material";
import ButtonWithProgress from "../../ButtonWithProgress/ButtonWithProgress";
import {makeStyles} from "@mui/styles";
import {useDispatch, useSelector} from "react-redux";
import {createWarehouseRequest} from "../../../../store/actions/warehousesActions";

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2)
  },
  alert: {
    marginTop: theme.spacing(3),
    width: "100%"
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));
const WarehouseAddForm = ({modalClose}) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const error = useSelector(state => state.warehouses.createWarehouseError);
  const loading = useSelector(state => state.warehouses.createWarehouseLoading);
  const [state, setState] = useState({
    name: ""
  });

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const submitFormHandler = async e => {
    e.preventDefault();
    await dispatch(createWarehouseRequest({...state}));
    if(!error) {
      return null;
    } else {
      modalClose();
    }
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  }

  return (
    <Stack spacing={2} maxWidth={700} width={'100%'}>
      <Typography variant={"h4"} textAlign={"center"}>
        Создать склад
      </Typography>
      <Grid
        container
        direction="column"
        spacing={2}
        component="form"
        className={classes.root}
        autoComplete="off"
        onSubmit={submitFormHandler}
        noValidate
      >
        <Grid item xs={12}>
          <TextField fullWidth color={'action1'}
                     label="Название"
                     name="name"
                     type="text"
                     value={state.name}
                     onChange={inputChangeHandler}
                     required
                     error={Boolean(getFieldError('name'))}
                     helperText={getFieldError('name')}
          />
        </Grid>
       <Grid item xs={12}>
         <ButtonWithProgress
           type="submit"
           fullWidth
           variant="contained"
           color={'action1'}
           className={classes.submit}
           loading={loading}
           disabled={loading}
         >
           Создать
         </ButtonWithProgress>
       </Grid>
      </Grid>
    </Stack>
  );
};

export default WarehouseAddForm;