import React from 'react';
import c from './Footer.module.css';
import Recall from "../Recall/Recall";

const Footer = () => {
  return (
    <footer>
      <div className={`${c.footer_data} container`}>
        <div className={c.footer_address}>
          <h6>Как нас найти?</h6>
          <ul>
            <li>Москва 105264, Верхняя первомайская ул. д.36,стр 1</li>
            <li className={c.time}>Время работы цеха c 10 до 19 Пн-Пт</li>
            <li className={c.time}>Консультации c 09 до 21 без выходных
            </li>
          </ul>
        </div>
        <div className={c.footer_contacts}>
          <h6>Контакты</h6>
          <ul>
            <li>Договорной отдел +7 (495) 969-00-26</li>
            <li >Позвонить в сервис +7 (926) 214-26-06</li>
            <li>+7 (926) 214-26-06</li>
          </ul>
        </div>
        <div>
          <Recall/>
        </div>
      </div>
      <div className={c.footer_date}>
          <h6>© 2022 Transrebuild.ru</h6>
      </div>
    </footer>
  );
};

export default Footer;