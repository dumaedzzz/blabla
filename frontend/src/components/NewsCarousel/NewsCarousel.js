import React, {useMemo} from 'react';
import {styled} from '@mui/styles';
import {Carousel} from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import c from "./NewsCarousel.module.css";
import {Button, Grid, Stack, Typography} from "@mui/material";
import Box from "@mui/material/Box";
import rightArrow from '../../assets/images/mainPagePictures/newsBtnArrowRight.svg';

const MoreButton = styled(Button)(() => ({
  textTransform: 'none',
  fontWeight: 500,
  fontSize: 14,
  lineHeight: '17px',
  color: '#5072A7',
}));

const NewsCarousel = ({news, columnSize = 1, handleOpen}) => {

  const adaptiveSizeColumnItems = useMemo(() => {
    const containers = [];
    for (let n = 0; n < news.length; n = n + columnSize) {
      let newsArr = [];
      for (let i = n; i < n + columnSize; i++) {
        if (news[i]) {
          newsArr.push(
            <Grid item key={news[i]._id} xs={12 / columnSize}>
              <Stack textAlign={"left"}
                     p={1} spacing={2}
                     alignItems={"flex-start"}
              >
                <Typography variant={'h4'} component={'h4'}
                            className={c.NewsItemTitle}
                            noWrap
                >
                  {news[i].title}
                </Typography>
                <Typography variant={'body1'} component={'p'}
                            className={c.NewsItemText}
                            height={80}
                >
                  {news[i].text}
                </Typography>
                <MoreButton
                  onClick={() => handleOpen(news[i])}
                  endIcon={<img src={rightArrow} alt="pic"/>}
                >
                  Подробнее
                </MoreButton>
              </Stack>
            </Grid>
          )
        } else {
          newsArr.push(null);
        }
      }
      containers.push(
        <Grid container
              key={n + 'newsCarousel'}
              justifyContent={"center"}
              flexWrap={"nowrap"}
              px={3} pb={6}
              spacing={2}
        >
          {newsArr}
        </Grid>
      )
    }
    return containers;
  }, [news, columnSize, handleOpen]);

  return (
    <Box sx={{
      background: "#FEFEFE",
      borderRadius: '30px',
      boxShadow: '0px 4px 27px 5px rgba(0, 0, 0, 0.05)',
    }}
         pt={4}
         pb={2}
    >
      <Carousel showThumbs={false}>
        {adaptiveSizeColumnItems}
      </Carousel>
    </Box>
  );
};

export default NewsCarousel;

