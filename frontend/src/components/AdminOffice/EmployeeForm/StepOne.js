import React from 'react';
import {Grid, TextField} from "@mui/material";

const StepOne = (props) => {
	return (
		<>
			<Grid item xs={12}>
				<TextField
					type="text"
					required
					fullWidth
					label="ФИО"
					name="name"
					value={props.employee.name}
					onChange={props.inputChangeHandler}
					error={Boolean(props.getFieldError('name'))}
					helperText={props.getFieldError('name')}
				/>
			</Grid>
			<Grid item xs={12}>
				<TextField
					type="number"
					required
					fullWidth
					label="ИНН"
					name="INN"
					value={props.employee.INN}
					onChange={props.inputChangeHandler}
					error={Boolean(props.getFieldError('INN'))}
					helperText={props.getFieldError('INN')}
				/>
			</Grid>
			<Grid item xs={12}>
				<TextField
					type="text"
					required
					fullWidth
					label="Номер телефона"
					name="phoneNumber"
					value={props.employee.phoneNumber}
					onChange={props.inputChangeHandler}
					placeholder='0(XXX)-XX-XX-XX'
					error={Boolean(props.getFieldError('phoneNumber'))}
					helperText={props.getFieldError('phoneNumber')}
				/>
			</Grid>
			<Grid item xs={12}>
				<TextField
					type="text"
					fullWidth
					label="Адрес проживания"
					name="address"
					value={props.employee.address}
					onChange={props.inputChangeHandler}
				/>
			</Grid>
			<Grid item xs={12}>
				<TextField
					type="file"
					fullWidth
					name="avatar"
					onChange={props.fileChangeHandler}
				/>
			</Grid>
		</>
	);
};

export default StepOne;