import React, {useEffect, useState} from 'react';
import {Grid} from "@mui/material";
import {BsFillShieldLockFill} from "react-icons/bs";
import {FaPassport} from "react-icons/fa";
import {RiPhoneFill} from "react-icons/ri";
import {MdLocationPin, MdEmail} from "react-icons/md";
import {useStyles} from './EmployeeForm.styles';
import defaultAvatar from '../../../assets/images/employeeImages/default_avatar.png'

const StepFinish = ({employee}) => {
	const classes = useStyles();
	const [photoPreview, setPhotoPreview] = useState(null) ;

	useEffect(() => {
		const IMG_TYPES = ['image/png', 'image/jpeg', 'image/jpg'];

		if (employee.avatar && IMG_TYPES.includes(employee.avatar.type)) {
			const render = new FileReader();

			render.onloadend = () => {
				setPhotoPreview(render.result);
			};

			render.readAsDataURL(employee.avatar)
		}
	}, []);

	return (
		<Grid
			container
			direction="column"
			className={classes.finishContainer}
		>
			<div
				className={classes.employeePhoto}
				style={{
					background: photoPreview ?
						`url('${photoPreview}') no-repeat center/cover` :
						`url('${defaultAvatar}') no-repeat center/cover`
				}}>
			</div>
			<h3>{employee.name}</h3>
			<h4>{employee.role !== '' && ((employee.role === 'repairer') ? 'Мастер' : 'Складовщик')}</h4>
			{(employee.warehouses.length !== 0) &&
				<ul>Список доступных складов:
					{employee.warehouses.map(w => (
						<li key={w} className={classes.warehousesList}>
							{w}
						</li>
					))}
				</ul>}
			{employee.workshop_id && <p>Мастерская: {employee.workshop_id.title ? employee.workshop_id.title : employee.workshop_id}</p>}
			<p><FaPassport/> {employee.INN}</p>
			<p><RiPhoneFill/> {employee.phoneNumber}</p>
			{employee.address && <p><MdLocationPin/>{employee.address}</p>}
			<p><MdEmail/> {employee.email}</p>
			<p><BsFillShieldLockFill/> {employee.password}</p>
		</Grid>
	);
};

export default StepFinish;