import {makeStyles} from "@mui/styles";

export const useStyles = makeStyles({
	root: {
		margin: '125px 0 25px',
		'& .button': {
		},
		'& .MuiStepIcon-root.Mui-active': {
			color: '#1D1D1F',
			fontSize: '34px',
		},
		'& .MuiStepIcon-root.Mui-completed': {
			color: 'green',
			fontSize: '34px',
		},
		'& .css-1u4zpwo-MuiSvgIcon-root-MuiStepIcon-root': {
			fontSize: '34px',
		},
		'& .css-zpcwqm-MuiStepConnector-root': {
			top: '18px',
			left: 'calc(-50% + 30px)',
			right: 'calc(50% + 30px)',
		},
		'& .css-z7uhs0-MuiStepConnector-line': {
			borderTopWidth: '2px',
		},
	},
	connectorActive: {
		'& $line': {
			borderColor: 'green',
		},
	},
	connectorCompleted: {
		'& $line': {
			borderColor: 'green',
		},
	},
	line: {},
	stepContainer: {
		marginBottom: '45px',
	},
	formContainer: {
		maxWidth: '500px',
		backgroundColor: '#FEFEFE',
		border: '1px solid #c1c1c1',
		borderRadius: '8px',
		padding: '30px',
		margin: '0 auto',
	},
	finishContainer: {
		padding: '30px 30px 0',
		lineHeight: '28px',
		'& h3, h4': {
			textAlign: 'center',
		},
	},
	leftIcon: {
		fontSize: '30px',
		color: 'green !important',
	},
	prevNextBtn: {
		width: '100%',
		display: 'flex',
		justifyContent: 'space-between',
		margin: '40px 0 0 16px',
		'& svg': {
			fontSize: '30px',
			color: 'green',
		},
		'& :only-child': {
			margin: '0 0 0 auto',
		},
	},
	employeePhoto: {
		width: '90px',
		height: '90px',
		borderRadius: '50%',
		margin: '0 auto 25px',
	},
	warehousesList: {
		listStyleType: "none",
		marginLeft: '30px'
	},
	finishButtons: {
		display: 'flex',
		flexWrap: 'wrap',
		justifyContent: 'space-between',
		margin: '40px 0 0 auto',
	},
});