import React from 'react';
import {Grid, TextField} from "@mui/material";

const StepTwo = (props) => {
	return (
		<>
			<Grid item xs={12}>
				<TextField
					type="email"
					required
					fullWidth
					label="Почта"
					name="email"
					value={props.employee.email}
					onChange={props.inputChangeHandler}
					error={Boolean(props.getFieldError('email'))}
					helperText={props.getFieldError('email')}
				/>
			</Grid>
			<Grid item xs={12}>
				<TextField
					type="password"
					required
					fullWidth
					label="Пароль"
					name="password"
					value={props.employee.password}
					onChange={props.inputChangeHandler}
					error={Boolean(props.getFieldError('password'))}
					helperText={props.getFieldError('password')}
				/>
			</Grid>
			<Grid item xs={12}>
				<TextField
					type="password"
					required
					fullWidth
					label="Потвердить пароль"
					name="confirmPassword"
					value={props.confirmPassword}
					onChange={props.changeConfirmPassword}
					error={props.passwordsMath}
					helperText={props.passwordsMath === false ? '' : 'Пароль не совпадает'}
				/>
			</Grid>
		</>
	);
};

export default StepTwo;