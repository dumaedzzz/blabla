import React, {useEffect, useState} from 'react';
import {Checkbox, FormControl, Grid, InputLabel, ListItemText, OutlinedInput, Select, TextField} from "@mui/material";
import MenuItem from "@mui/material/MenuItem";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
	PaperProps: {
		style: {
			maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
			width: 250,
		},
	},
};

const StepThree = (props) => {
	// For example:
	const workshops = [
		{_id: 1, title: 'Мастерская-1', status: 'active', warehouse_id: '3'},
		{_id: 2, title: 'Мастерская-2', status: 'inactive', warehouse_id: '2'},
		{_id: 3, title: 'Мастерская-3', status: 'active', warehouse_id: '1'}
	]; // GET/allWorkshops
	const warehouses = [
		{_id: 1545, title: 'Склад-1', status: 'active', warehouseman_id: '1545444454'},
		{_id: 214, title: 'Склад-2', status: 'active', warehouseman_id: ''},
		{_id: 384, title: 'Склад-3', status: 'inactive', warehouseman_id: ''},
		{_id: 4245, title: 'Склад-4', status: 'active', warehouseman_id: 'f3ds3fd4454'},
	]; // GET/ get all warehouses that are not connected to warehouseman ????

	const roles = [{titleEng: 'warehouseman', titleRu: 'Складовщик'}, {titleEng: 'repairer', titleRu: 'Мастер'}];
	const [getDisabled, setDisabled] = useState(false);

	useEffect(() => {
		if (props.employee.role !== '') {
			setDisabled(true);
		}
	}, []);

	const handleChange = value => {
		props.setEmployee(prev => ({
			...prev,
			warehouses: typeof value === 'string' ? value.split(',') : value,
		}));
	};

	const changeRole = value => {
		props.setEmployee(prevState => ({...prevState, role: value}));

		if (value === 'warehouseman') {
			props.setEmployee(prevState => ({...prevState, workshop_id: null}));
		}

		if (value === 'repairer') {
			props.setEmployee(prev => ({
				...prev,
				warehouses: [],
			}));
		}
	};

	const workshopField = (
		<Grid item xs={12}>
			<TextField
				type="text"
				select
				fullWidth
				label="Мастерская"
				name="workshop_id"
				value={props.employee.workshop_id === null ? '' : props.employee.workshop_id}
				onChange={props.inputChangeHandler}
			>
				{workshops.map(workshop => (
					<MenuItem
						key={workshop._id}
						value={workshop._id}
					>
						{workshop.title}
					</MenuItem>
				))}
			</TextField>
		</Grid>
	);

	const warehouseField = (
		<Grid item xs={12}>
			<FormControl fullWidth>
				<InputLabel id="warehouse-multiple-checkbox-label">Склады</InputLabel>
				<Select
					labelId="warehouse-multiple-checkbox-label"
					id="warehouse-multiple-checkbox"
					multiple
					value={props.employee.warehouses}
					onChange={e => handleChange(e.target.value)}
					input={<OutlinedInput label="Склады" />}
					renderValue={(selected) => selected.join(', ')}
					MenuProps={MenuProps}
				>
					{warehouses.map(name=> (
						<MenuItem key={name._id} value={name.title}>
							<Checkbox checked={props.employee.warehouses.indexOf(name.title) > -1} />
							<ListItemText primary={name.title} />
						</MenuItem>
					))}
				</Select>
			</FormControl>
		</Grid>
	);

	return (
		<>
			<Grid item xs={12}>
				<TextField
					fullWidth
					required
					select
					variant="outlined"
					label="Должность"
					name="role"
					disabled={getDisabled}
					value={props.employee.role}
					onChange={e => changeRole(e.target.value)}
					error={Boolean(props.getFieldError('role'))}
					helperText={props.getFieldError('role')}
				>
					{roles.map(role => (
						<MenuItem
							key={role.titleEng}
							value={role.titleEng}
						>
							{role.titleRu}
						</MenuItem>
					))}
				</TextField>
			</Grid>
			{props.employee.role === '' ? null : (props.employee.role === 'repairer' ? workshopField : warehouseField)}
		</>
	);
};

export default StepThree;