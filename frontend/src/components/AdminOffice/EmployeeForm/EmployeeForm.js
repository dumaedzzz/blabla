import React, {useState} from 'react';
import {Stepper, Step, StepLabel, Grid, StepConnector, Typography} from "@mui/material";
import {useStyles} from './EmployeeForm.styles';
import {BsArrowRightCircle} from "react-icons/bs";
import {BsArrowLeftCircle} from "react-icons/bs";
import StepOne from "./StepOne";
import StepTwo from "./StepTwo";
import StepThree from "./StepThree";
import StepFinish from "./StepFinish";
import ButtonWithProgress from "../../UI/ButtonWithProgress/ButtonWithProgress";

const initialState = {
	name: '',
	INN: '',
	phoneNumber: '',
	address: '',
	avatar: null,
	email: '',
	password: '',
	role: '',
	warehouses: [],
	workshop_id: null,
};

const EmployeeForm = ({onSubmit, employeeData, error, loading}) => {
	const classes = useStyles();

	const steps = ['Персональные данные', 'Создание пароля', 'Должность'];
	const [activeStep, setActiveStep] = useState(0);
	const [confirmPassword, setConfirmPassword] = useState(employeeData ? employeeData.password : '');
	const [passwordsMath, setPasswordsMatch] = useState(false);

	const [employee, setEmployee] = useState( employeeData || initialState);

	const inputChangeHandler = e => {
		const {name, value} = e.target;
		setEmployee(prevState => ({...prevState, [name]: value}));
	};

	const fileChangeHandler = e => {
		const name = e.target.name;
		const file = e.target.files[0];
		setEmployee(prevState => {
			return {...prevState, [name]: file};
		});
	};

	const changeConfirmPassword = e => {
		setConfirmPassword(e.target.value);
	};

	const submitFormHandler = e => {
		e.preventDefault();

		const formData = new FormData();
		Object.keys(employee).forEach(key => {
			formData.append(key, employee[key]);
		});

		onSubmit(formData);
	};

	const getFieldError = fieldName => {
		try {
			return error.errors[fieldName].message;
		} catch (e) {
			return undefined;
		}
	};

	const handleNext = () => {
		if (confirmPassword !== employee.password) {
			return setPasswordsMatch(true);
		}

		setActiveStep(prevState => prevState + 1);
	};

	const handlePrev = () => {
		setActiveStep(prevState => prevState - 1);
	};

	const getStepsContent = stepIndex => {
		switch (stepIndex) {
			case 0:
				return <StepOne
					employee={employee}
					inputChangeHandler={inputChangeHandler}
					fileChangeHandler={fileChangeHandler}
					getFieldError={getFieldError}
				/>;
			case 1:
				return <StepTwo
					employee={employee}
					inputChangeHandler={inputChangeHandler}
					confirmPassword={confirmPassword}
					changeConfirmPassword={changeConfirmPassword}
					passwordsMath={passwordsMath}
					getFieldError={getFieldError}
				/>;
			case 2:
				return <StepThree
					employee={employee}
					setEmployee={setEmployee}
					inputChangeHandler={inputChangeHandler}
					getFieldError={getFieldError}
				/>;
			default:
				return "Unknown Step";
		}
	};

	return (
		<div className="container">
			<div className={classes.root}>
				{employeeData ?
					<Typography component="h1" variant="h5" mb={4} style={{textAlign: 'center'}}>
						Редактирование сотрудника
					</Typography> :
					<Typography component="h1" variant="h5" mb={4} style={{textAlign: 'center'}}>
						Добавление нового сотрудника
					</Typography>
				}
				<Stepper
					alternativeLabel
					activeStep={activeStep}
					connector={
						<StepConnector
							classes={{
								active: classes.connectorActive,
								completed: classes.connectorCompleted,
								line: classes.line,
							}}
						/>
					}
					className={classes.stepContainer}
				>
					{steps.map(step => (
						<Step key={step}>
							<StepLabel>{step}</StepLabel>
						</Step>
					))}
				</Stepper>
				<div className={classes.formContainer}>
					<Grid
						container
						component="form"
						onSubmit={submitFormHandler}
						spacing={2}
						noValidate
					>
						{activeStep === steps.length ?
							<StepFinish employee={employee}/> :
							<>{getStepsContent(activeStep)}</>
						}
						<div className={classes.prevNextBtn}>
							{((activeStep !== 0) ) && <BsArrowLeftCircle onClick={handlePrev}/>}
							{(activeStep < steps.length) ? <BsArrowRightCircle onClick={handleNext}/> :
								<ButtonWithProgress
									type="submit"
									variant="contained"
									style={{backgroundColor: "#4355FB"}}
									loading={loading}
									disabled={loading}
								>
									{employeeData ? 'Редактировать' : 'Создать'}
								</ButtonWithProgress>
							}
						</div>
					</Grid>
				</div>
			</div>
		</div>
);
};

export default EmployeeForm;