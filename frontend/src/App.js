import React from 'react';
import Layout from './components/UI/AppBar/Layout/Layout';
import Main from './containers/MainPages/Main/Main';
import AuthPage from './containers/MainPages/AuthPage/AuthPage';
import {Route, Routes} from "react-router-dom";
import CreateOrder from "./containers/OrderPage/CreateOrder/CreateOrder";
import AdminPage from "./containers/MainPages/AdminPage/AdminPage";
import OrderPage from "./containers/OrderPage/OrderPage";
import WorkShopPage from "./containers/MainPages/WorkShopPage/WorkShopPage";
import AddEmployee from "./containers/MainPages/AdminPage/Employees/AddEmployee/AddEmployee";
import EditEmployee from "./containers/MainPages/AdminPage/Employees/EditEmployee/EditEmployee";
import AdminEmployeePage from "./containers/MainPages/AdminPage/Employees/AdminEmployeePage";
import NewsForm from "./components/UI/NewsForm/NewsForm";
import IncomeDetails from "./containers/MainPages/IncomeDetails/IncomeDetails";

const App = () => {

  return (
    <Layout>
      <Routes>
        <Route path="/" element={<Main/>}/>
        <Route path="/login" element={<AuthPage/>}/>
        <Route path="/orderPage" element={<OrderPage/>}/>
        <Route path="/createOrder" element={<CreateOrder/>}/>
        <Route path="/admin" element={<AdminPage/>}/>
        <Route path="/create-employee" element={<AddEmployee/>}/>
        <Route path="/edit-employee" element={<EditEmployee/>}/>
        <Route path="/create-news" element={<NewsForm/>}/>
        <Route path="/change-news/:id" element={<NewsForm/>}/>
        <Route path="/employee" element={<AdminEmployeePage/>}/>
        <Route path="/workshop" element={<WorkShopPage/>}/>
	      <Route path="/income-details" element={<IncomeDetails/>}/>
        <Route element={() => <h1>Not found</h1>}/>
      </Routes>
    </Layout>
  );
};

export default App;


