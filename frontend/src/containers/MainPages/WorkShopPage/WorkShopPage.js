import React from 'react';
import {Grid, InputBase} from "@mui/material";
import c from "./WorkShopPage.module.css";
import SearchIcon from "@mui/icons-material/Search";
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles(theme => {
  return ({
    search: {
      position: 'relative',
      borderRadius: theme.shape.borderRadius,
      backgroundColor: "rgba(209, 209, 209, 0.4)",
      '&:hover': {
        backgroundColor: "rgba(209, 209, 209, 0.4)",
      },
      [theme.breakpoints.up('md')]: {
        width: 'auto',
        marginLeft: '30px',
      },
      [theme.breakpoints.up('xs')]: {
        marginBottom: "30px",
      },
      ['@media (min-width:768px)']: {
        marginBottom: "0",
      },
    },
    searchIcon: {
      top: 0,
      left: "76%",
      padding: theme.spacing(0, 2),
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      borderLeft: `1px solid #1D1D1F`
    }
  });
})

const WorkShopPage = () => {
  const classes = useStyles();
  return (
    <Grid className="container" container lg={10} xs={12}>
      <Grid>
        <div className={classes.search}>
          <InputBase
            placeholder="Введите код"
            className={c.inputSearch}
            inputProps={{'aria-label': 'search'}}
          />
          <div className={classes.searchIcon}>
            <SearchIcon style={{color: "rgba(255, 255, 255, 0.5)"}}/>
          </div>
        </div>
      </Grid>
    </Grid>
  );
};

export default WorkShopPage;