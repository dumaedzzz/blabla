import React, {useEffect, useState} from 'react';
import {Button, CircularProgress, Grid, Paper, TextField} from "@mui/material";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {fetchEmployees} from "../../../../store/actions/employeesActions";
import Box from "@mui/material/Box";

const AdminEmployeePage = () => {
	const dispatch = useDispatch();
	const employees = useSelector(state => state.employees.employees);
	const loading = useSelector(state => state.employees.fetchEmployeesLoading);
	const [search, setSearch] = useState('');

	useEffect(() => {
		dispatch(fetchEmployees());
	}, [dispatch])

	return (
		<div className="container">
			<Grid container justifyContent="space-between" pt={5} mb={5}>
				<Grid item xs={6}>
					<TextField
						type="text"
						fullWidth
						label="Поиск"
						name="search"
						value={search}
						onChange={e => setSearch(e.target.value)}
					/>
				</Grid>
				<Grid item>
					<Button
						component={Link}
						to={'/create-employee'}
						variant="contained"
						style={{backgroundColor: "#4355FB"}}
					>
						Создать
					</Button>
				</Grid>
			</Grid>
			<div>
				{loading ?
					<Grid container justifyContent="center" alignItems="center">
						<Grid item>
							<CircularProgress/>
						</Grid>
					</Grid> :
					employees.filter(value => {
						if (search === '') {
							return value;
						} else if (value.name.toLowerCase().includes(search.toLowerCase())) {
							return value;
						}

					}).map(employee => (
						<Paper key={employee._id}>
							<Box mb={3} p={2}>
								{employee.name}
								<Button component={Link} to={`/edit-employee?employee=${employee._id}`}>Edit</Button>
							</Box>
						</Paper>
					))
				}
			</div>
		</div>
	);
};

export default AdminEmployeePage;