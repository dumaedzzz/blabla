import React from 'react';
import EmployeeForm from "../../../../../components/AdminOffice/EmployeeForm/EmployeeForm";
import {useDispatch, useSelector} from "react-redux";
import {createEmployee} from "../../../../../store/actions/employeesActions";

const AddEmployee = () => {
	// const user = useSelector(state => state.users.user) if (user ? (user.role === admin)) then open this page
	const dispatch = useDispatch();
	const error = useSelector(state => state.employees.createEmployeeError);
	const loading  = useSelector(state => state.employees.createEmployeeLoading);

	const onSubmit = newEmployeeData => {
		dispatch(createEmployee(newEmployeeData));
	};

	return (
		<EmployeeForm onSubmit={onSubmit} error={error} loading={loading}/>
	);
};

export default AddEmployee;