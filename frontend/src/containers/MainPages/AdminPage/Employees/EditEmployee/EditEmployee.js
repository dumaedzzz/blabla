import React, {useEffect} from 'react';
import {useLocation} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import EmployeeForm from "../../../../../components/AdminOffice/EmployeeForm/EmployeeForm";
import {editEmployee, fetchEmployee} from "../../../../../store/actions/employeesActions";
import {CircularProgress, Grid} from "@mui/material";

const EditEmployee = () => {
 const location = useLocation();
 const dispatch = useDispatch();
 const employee = useSelector(state => state.employees.employee);
 const loading = useSelector(state => state.employees.fetchEmployeeLoading);
 const editLoading = useSelector(state => state.employees.editEmployeeLoading);
 const editError = useSelector(state => state.employees.editEmployeeError);

	useEffect(() => {
		dispatch(fetchEmployee(location.search));
	}, [location.search]);

	const onSubmit = employeeData => {
		dispatch(editEmployee({id: location.search, data: employeeData}));
	};

	return loading ?
			<Grid container justifyContent="center" alignItems="center">
				<Grid item>
					<CircularProgress/>
				</Grid>
			</Grid> :
		<EmployeeForm
			onSubmit={onSubmit}
			employeeData={employee}
			loading={editLoading}
			error={editError}
		/>
};

export default EditEmployee;