import React, {useEffect, useState} from 'react';
import InpSearch from "../../../../components/UI/Inputs/InpSearch/InpSearch";
import {Backdrop, Button, CircularProgress, Fade, Grid, IconButton, Modal, Paper, Stack} from "@mui/material";
import WarehouseItem from "./Warehouse/WarehouseItem";
import Box from "@mui/material/Box";
import CloseIcon from '@mui/icons-material/Close';
import WarehouseAddForm from "../../../../components/UI/Forms/WarehouseAddForm/WarehouseAddForm";
import {useStyles} from "./Modal.styles";
import {useDispatch, useSelector} from "react-redux";
import {fetchWarehousesRequest} from "../../../../store/actions/warehousesActions";


const WarehouseMainPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const warehouses = useSelector(state => state.warehouses.warehouses);
  const fetchLoading = useSelector(state => state.warehouses.fetchWarehousesLoading);
  const [value, setValue] = useState('');

  useEffect(() => {
    dispatch(fetchWarehousesRequest());
  }, []);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const filteredWarehouses = warehouses.filter(warehouse => {
    return warehouse.name.toLowerCase().includes(value.toLowerCase())
  })

  return (
    <Stack spacing={3} my={2} width={'100%'}>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <WarehouseAddForm
              modalClose={handleClose}
            />
            <IconButton onClick={handleClose} className={classes.close}>
              <CloseIcon/>
            </IconButton>
          </div>
        </Fade>
      </Modal>
      <Grid container
            alignItems={"center"}
            justifyContent={"space-between"}
            flexWrap={"wrap-reverse"}
            spacing={2}
      >
        <Grid item xs={12} sm={6}>
          <InpSearch
            type="text"
            placeholder={"Поиск склада"}
            onChange={(event) => setValue(event.target.value)}
          />

        </Grid>
        <Grid item xs={12} sm={5} md={3}>
          <Button variant={"contained"}
                  fullWidth
                  onClick={handleOpen}
          >
            Создать склад
          </Button>
        </Grid>
      </Grid>

      {fetchLoading ? (
          <Grid container justifyContent="center" alignItems="center">
            <Grid item>
              <CircularProgress/>
            </Grid>
          </Grid>
        ) : filteredWarehouses.map(warehouse => (
        <Box component={Paper} p={2}
          key={warehouse._id}
        >
          <WarehouseItem
            id={warehouse._id}
            name={warehouse.name}
            status={warehouse.status}
          />
        </Box>
      ))
      }
    </Stack>
  );
};

export default WarehouseMainPage;