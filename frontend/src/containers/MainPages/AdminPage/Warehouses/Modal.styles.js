import {makeStyles} from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    position: 'relative',
    height: '350px',
    width: '500px',
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(6, 5, 2),
    paddingTop: '80px',
    display: 'inline-block'
  },
  close: {
    position: 'absolute',
    right: '20px',
    top: '20px',
  },
  status: {
    fontWeight: 'bold',
    fontSize: '0.75rem',
    color: '#fff',
    backgroundColor: '#ccc',
    borderRadius: '8px',
    padding: '3px 10px',
    display: 'inline-block',
  },

}))