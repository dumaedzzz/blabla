import React, {useState} from 'react';
import {Backdrop, Button, Fade, Grid, IconButton, Modal, Typography} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import WarehouseEditForm from "../../../../../components/UI/Forms/WarehouseEditForm/WarehouseEditForm";
import {useStyles} from "../Modal.styles";
import {Link} from "react-router-dom";

const WarehouseItem = ({name, id, status}) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  return (
    <Grid container spacing={3}
          justifyContent={"space-between"}
          alignItems={"center"}
    >
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <WarehouseEditForm
              id={id}
              name={name}
              modalClose={handleClose}
              status={status}
            />
            <IconButton onClick={handleClose} className={classes.close}>
              <CloseIcon/>
            </IconButton>
          </div>
        </Fade>
      </Modal>
      <Grid item xs={12} sm={"auto"} textAlign={"center"}>
        <Typography variant={"body1"}>
          {name}
        </Typography>
      </Grid>

      {status ?
        <Grid item xs={12} sm={"auto"} textAlign={"center"}>
          <Typography
            className={classes.status}
            style={{
              backgroundColor: 'green'
            }}
          >
            Активный
          </Typography>
        </Grid> :
        <Grid item xs={12} sm={"auto"} textAlign={"center"}>
          <Typography
            className={classes.status}
            style={{
              backgroundColor: 'red'
            }}
          >
            Неактивный
          </Typography>
        </Grid>}

      <Grid item container spacing={1} xs={12} sm={"auto"}>
        <Grid item xs={6} sm={'auto'}>
          <Button variant={"contained"} fullWidth component={Link} to={`/income-details?warehouse=${id}`}>
            Приход
          </Button>
        </Grid>
        <Grid item xs={6} sm={'auto'}>
          <Button variant={"contained"}
                  fullWidth
                  onClick={handleOpen}
          >
            Редактировать
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default WarehouseItem;