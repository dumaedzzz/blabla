import React, {useState} from 'react';
import {Avatar, Grid, Stack, TextField, Typography, useMediaQuery} from "@mui/material";
import VpnKeyIcon from '@mui/icons-material/VpnKey';
import {useTheme} from "@mui/styles";
import {LoadingButton} from "@mui/lab";
import {useDispatch} from "react-redux";
import {registerUserFailure} from "../../../../store/actions/usersActions";
import Box from "@mui/material/Box";

const initialState = {
  email: '',
  password: '',
  confirmPassword: '',
  name: '',
  phone: '',
  address: '',
  avatar: '',
}

const RegisterForm = ({handleSubmit, errSignUp, loadingSubmit}) => {
  const theme = useTheme();
  const isSm600down = useMediaQuery(theme.breakpoints.down('sm'));
  const dispatch = useDispatch();

  const [newUserData, setNewUserData] = useState(initialState);

  const inpChanger = (e) => {
    if (errSignUp?.errors) {
      for (let key of Object.keys(errSignUp.errors)) {
        if (errSignUp.errors[key]?.['message'] && (key === e.target.name)) {
          dispatch(registerUserFailure({...errSignUp, errors: {...errSignUp.errors, [e.target.name]: null}}));
        }
      }
    }
    setNewUserData(prevState => ({...prevState, [e.target.name]: e.target.value}));
  };
  const inpImgChanger = (e) => {
    setNewUserData(prevState => ({...prevState, [e.target.name]: e.target.files[0]}));
  }

  const onSubmit = (e) => {
      e.preventDefault();
    if (newUserData.password !== newUserData.confirmPassword) {
      if (errSignUp) {
        dispatch(registerUserFailure({
          ...errSignUp,
          errors: {...errSignUp.errors, password: {message: 'Ваш пароль не совпадает'}}
        }
        ));
      } else {
        dispatch(registerUserFailure(
          {errors: {password: {message: 'Ваш пароль не совпадает'}}}
        ));
      }
      return;
    }
    const newUserFormData = new FormData;
    Object.keys(newUserData).forEach(key => {
      newUserFormData.append(key, newUserData[key]);
    })
    handleSubmit(newUserFormData);
  }

  return (
    <Stack component={"form"}
           onSubmit={onSubmit}
           maxWidth={700}
           noValidate
           spacing={'5px'}
           mx={"auto"}
           my={2}
           px={1}
    >
      <Avatar sx={{m: '10px auto', bgcolor: 'primary.main'}}>
        <VpnKeyIcon/>
      </Avatar>
      <Typography component="h3"
                  variant="h5"
                  textAlign={"center"}
      >
        Регистрация
      </Typography>
      <TextField
        type={"email"}
        name={"email"}
        value={newUserData.email}
        error={Boolean(errSignUp?.errors?.email)}
        helperText={errSignUp?.errors?.email?.message ? errSignUp?.errors?.email?.message : ' '}
        label={'Почта'}
        onChange={inpChanger}
        required
        autoFocus
      />
      <TextField
        name={"name"}
        error={Boolean(errSignUp?.errors?.name)}
        helperText={errSignUp?.errors?.name?.message ? errSignUp?.errors?.name?.message : ' '}
        label={'Имя'}
        onChange={inpChanger}
        value={newUserData.name}
        required
      />
      <Grid container>
        <Grid item sm={6} xs={12} pr={{sm: 1}} pb={isSm600down ? 2 : 0}>
          <TextField
            type={"password"}
            name={"password"}
            error={Boolean(errSignUp?.errors?.password)}
            helperText={errSignUp?.errors?.password?.message ? errSignUp?.errors?.password?.message : ' '}
            label={'Пароль'}
            onChange={inpChanger}
            value={newUserData.password}
            required
            fullWidth
          />
        </Grid>
        <Grid item sm={6} xs={12}>
          <TextField
            type={"password"}
            name={"confirmPassword"}
            error={Boolean(errSignUp?.errors?.password)}
            helperText={errSignUp?.errors?.password?.message ? errSignUp?.errors?.password?.message : ' '}
            label={'Потвердите пароль'}
            onChange={inpChanger}
            value={newUserData.confirmPassword}
            required
            fullWidth
          />
        </Grid>
      </Grid>
      <Grid container>
        <Grid item sm={6} xs={12} pr={{sm: 1}} pb={isSm600down ? 2 : 0}>
          <TextField
            name={"phone"}
            error={Boolean(errSignUp?.errors?.phone)}
            helperText={errSignUp?.errors?.phone?.message ? errSignUp?.errors?.phone?.message : ' '}
            label={'Номер телефона'}
            onChange={inpChanger}
            value={newUserData.phone}
            required
            fullWidth
          />
        </Grid>
        <Grid item sm={6} xs={12}>
          <TextField
            name={"address"}
            error={Boolean(errSignUp?.errors?.address)}
            helperText={errSignUp?.errors?.address?.message ? errSignUp?.errors?.address?.message : ' '}
            label={'Адрес(Город)'}
            onChange={inpChanger}
            value={newUserData.address}
            required
            fullWidth
          />
        </Grid>
      </Grid>
      <TextField
        type={"file"}
        name={"avatar"}
        onChange={inpImgChanger}
      />
      <Box pt={2}>
        <LoadingButton loading={loadingSubmit}
                       type={"submit"}
                       variant={"contained"}
                       fullWidth
        >
          Подтвердить
        </LoadingButton>
      </Box>
    </Stack>
  );
};

export default RegisterForm;