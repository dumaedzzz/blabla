import React, {useEffect, useState} from 'react';
import {Checkbox, Container, FormControlLabel, Grid, Link, Typography} from "@mui/material";
import RegisterForm from "./RegisterForm/RegisterForm";
import LoginForm from "./LoginForm/LoginForm";
import {useDispatch, useSelector} from "react-redux";
import {
  logInEmployee,
  logInUser,
  logInUserFailure,
  registerUser,
  registerUserFailure
} from "../../../store/actions/usersActions";

const AuthPage = () => {
  const [isRegistered, setIsRegistered] = useState(true);
  const [isEmployee, setIsEmployee] = useState(false);
  const dispatch = useDispatch();
  const errSignUp = useSelector(state => state.users.errSignUp);
  const errLogIn = useSelector(state => state.users.errLogIn);
  const signUpLoading = useSelector(state => state.users.signUpLoading);
  const logInLoading = useSelector(state => state.users.logInLoading);

  const registrationSubmitHandler = (data) => {
    dispatch(registerUser(data));
  };

  const registerHandle = (e) => {
    e.preventDefault()
    setIsRegistered(prevState => !prevState)
  };
  const loginSubmitHandler = (data) => {
    isEmployee ? dispatch(logInEmployee(data)) : dispatch(logInUser(data))
  };

  useEffect(() => {
    if (errSignUp) {
      dispatch(registerUserFailure(null));
    }
    if (errLogIn) {
      dispatch(logInUserFailure(null));
    }
  }, [isRegistered])


  return (
    <Container sx={{padding: "20px 0"}}>
      {!isRegistered && (
        <RegisterForm
          handleSubmit={registrationSubmitHandler}
          errSignUp={errSignUp}
          loadingSubmit={signUpLoading}
        />
      )}
      {isRegistered && (
        <LoginForm
          errLogIn={errLogIn?.errors?.message}
          handleSubmit={loginSubmitHandler}
          submitLoading={logInLoading}
          resetError={() => dispatch(logInUserFailure(null))}
        />
      )
      }

      <Grid container
            textAlign={"center"}
            justifyContent={isRegistered ? "space-between" : "center"}
            alignItems={"center"}
            px={1}
            maxWidth={700}
            mx={"auto"}

      >
        <Grid item xs={12} sm={"auto"} my={1}>
          <Link onClick={registerHandle}
                textAlign={"center"}
                sx={{":hover": {cursor: "pointer"}}}
          >
            {isRegistered ? 'Еще нет аккаунта? Зарегистрируйтесь.' : 'Есть аккаунт? Выполните вход.'}
          </Link>
        </Grid>
        {isRegistered && (
          <Grid item xs={12} sm={"auto"}>
            <FormControlLabel
              control={
                <Checkbox
                  onClick={() => {
                    setIsEmployee(prevState => !prevState)
                  }}
                  checked={isEmployee}
                  color="success"
                />}
              label={<Typography variant={"subtitle2"}>Войти как сотрудник</Typography>}
              labelPlacement={'start'}
            />
          </Grid>
        )}
      </Grid>
    </Container>
  );
};

export default AuthPage;

