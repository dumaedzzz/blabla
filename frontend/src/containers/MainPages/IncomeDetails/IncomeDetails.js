import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import AccountsDetailForm from "../../../components/AccountsDetailForm/AccountsDetailForm";
import {fetchTypesDetailsRequest} from "../../../store/actions/typesDetailsActions";
import {fetchPartNumbersDetailsRequest} from "../../../store/actions/partNumbersDetailsActions";
import {addDetailRequest} from "../../../store/actions/detailsActions";
import {useLocation} from "react-router-dom";

const IncomeDetails = () => {
	const dispatch = useDispatch();
	const location = useLocation();
	const types = useSelector(state => state.typesDetails.types);
	const partNumbers = useSelector(state => state.partNumbersDetails.partNumbers);
	const fetchTypesLoading = useSelector(state => state.typesDetails.fetchTypesDetailsLoading);
	const fetchPartNumbersLoading = useSelector(state => state.partNumbersDetails.fetchPartNumbersDetailsLoading);

	useEffect(() => {
		dispatch(fetchTypesDetailsRequest());
		dispatch(fetchPartNumbersDetailsRequest());
	}, [dispatch]);

	const onSubmit = data => {
		dispatch(addDetailRequest({id: location.search, data}));
	};

	return (
		<div className="container">
			<AccountsDetailForm
				typeOfAction={'income'}
				types={types}
				partNumbers={partNumbers}
				fetchTypesLoading={fetchTypesLoading}
				fetchPartNumbersLoading={fetchPartNumbersLoading}
				onSubmit={onSubmit}
			/>
		</div>
	);
};

export default IncomeDetails;