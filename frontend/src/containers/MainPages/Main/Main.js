import React from 'react';
import c from './Main.module.css'
import {Button, Grid, TextField} from "@mui/material";
import aboutImage from '../../../assets/images/mainPagePictures/ic-gtd.png';
import phone from '../../../assets/images/mainPagePictures/ic-phone.png';
import delivery from '../../../assets/images/mainPagePictures/ic-delivery.png';
import hammer from '../../../assets/images/mainPagePictures/ic-hammer.png';
import handshake from '../../../assets/images/mainPagePictures/ic-handshake.png';
import trackDone from '../../../assets/images/mainPagePictures/ic-trackDone.png';
import Recall from "../../../components/UI/Recall/Recall";
import NewsBlock from "./NewsBlock/NewsBlock";
import {useStyles} from './Main.styles';
import MainTitles from "../../../components/UI/MainTitles/MainTitles";
import Footer from '../../../components/UI/Footer/Footer';


const Main = () => {
  const classes = useStyles();
  return (
    <>
      <div className={c.viewMore}>
        <div className={c.view}>
          <div className="container">
            <div className={`${c.mainTitleButton}`}>
              <p className={c.title}>Ремонт
                гидротрансформаторов</p>
              <Recall/>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <MainTitles title="гидротрансформатор?" span="Что такое"/>
        <div className={c.aboutBlock}>
          <p className={c.mainText}><span className={c.mainWords}>Гидротрансформатор или «бублик»</span>—
            составная часть АКПП и один из ее
            основных функциональных элементов. Он включает четыре основных части: насосное колесо, турбинное колесо,
            статор (реактор), а также механизм блокировки. Назначение бублика состоит в том, чтобы связывать трансмиссию
            с двигателем внутреннего сгорания.</p>
          <Grid item className={c.imageBlock}>
            <img src={aboutImage} alt="Gdp" className={classes.image}/>
          </Grid>
        </div>
        <MainTitles title="Этапы работы"/>
        <div className={c.etapsWork}>
          <div className={c.item}>
            <img src={phone} alt="phone" className={classes.phone}/>
            <p className={c.itemText}><span className={classes.phoneSpan}>Заказываете </span>звонок оператора</p>
          </div>
          <div className={c.item}>
            <img src={delivery} alt="delivery" className={classes.delivery}/>
            <p className={c.itemText}><span className={classes.deliverySpan}>Отправляете</span> нам</p>
          </div>
          <div className={c.item}>
            <img src={hammer} alt="hammer" className={classes.hammer}/>
            <p className={c.itemText}>Ремонтируем</p>
          </div>
          <div className={c.item}>
            <img src={handshake} alt="handshake" className={classes.handshake}/>
            <p className={c.itemText}><span className={classes.handshakeSpan}>Сдаем</span> работу</p>
          </div>
          <div className={c.item}>
            <img src={trackDone} alt="trackDone" className={classes.trackDone}/>
            <p className={c.itemText}><span className={classes.trackDoneSpan}>Забираете</span> и устанавливаете
            </p>
          </div>
        </div>
        <MainTitles title="прямо сейчас" span="Проверь статус своего заказа"/>
        <div className={c.control}>
          <TextField value="" className={c.controlInput} label="Код проверки"/>
          <Button className={c.controlButton} color="primary" variant="contained">Проверить</Button>
        </div>
        <NewsBlock/>
      </div>
      <Footer/>
    </>
  );
};

export default Main;