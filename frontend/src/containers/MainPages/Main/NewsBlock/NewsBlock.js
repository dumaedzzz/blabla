import React, {useEffect, useState} from 'react';
import NewsCarousel from "../../../../components/NewsCarousel/NewsCarousel";
import {
  Backdrop,
  Box, Button,
  Card, CardActions,
  CardContent,
  CardMedia, CircularProgress,
  Fade, Grid,
  Modal,
  Stack,
  Typography,
  useMediaQuery
} from "@mui/material";
import {useTheme} from "@mui/styles";
import {useDispatch, useSelector} from "react-redux";
import {fetchNewsRequest} from "../../../../store/actions/newsActions";
import useStyles from './styles';
import DeleteIcon from '@mui/icons-material/Delete';
import SettingsIcon from '@mui/icons-material/Settings';
import {BASE_URL} from "../../../../config";
import {Link} from "react-router-dom";
import {removeNewsRequest} from '../../../../store/actions/newsActions';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  maxWidth: "500px",
  fontFamily: 'Montserrat, sans-serif',
  background: '#FBFBFB',
  boxShadow: 24,
  borderRadius: "4% 4% 0 0",
};


const NewsBlock = () => {
  const classes = useStyles();
  const theme = useTheme();
  const [openNews, setOpenNews] = useState(null);
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();
  const {profile} = useSelector(state => state.users)
  const {news, fetchNewsLoading} = useSelector(state => state.news);
  const isSm600down = useMediaQuery(theme.breakpoints.down('sm'));
  const isMd900between = useMediaQuery(theme.breakpoints.between('sm', 'md'));
  const isMd900up = useMediaQuery(theme.breakpoints.up('md'));

  useEffect(() => {
    dispatch(fetchNewsRequest());
  }, [dispatch]);


  const handleOpen = (newsData) => {
    setOpen(true);
    setOpenNews(newsData)
  }
  const handleClose = () => {
    setOpen(false);
    setOpenNews(null)
  }

  let image = null;
  if(openNews?.image){
    image = BASE_URL + '/' + openNews?.image;
  }


  const removeHandler = (id) => {
    dispatch(removeNewsRequest(id));
  }


  return fetchNewsLoading ? (
    <Grid container justifyContent="center" alignItems="center">
      <Grid item>
        <CircularProgress/>
      </Grid>
    </Grid>
  ) : (
    <Stack my={5} spacing={2}>
      <Typography variant={'h3'}
                  textAlign={'center'}
      >
        Новости
      </Typography>
      {isSm600down && (
        <NewsCarousel
          handleOpen={handleOpen}
          news={news}
        />
      )}
      {isMd900between && (
        <NewsCarousel
          handleOpen={handleOpen}
          columnSize={2}
          news={news}
        />
      )}
      {isMd900up && (
        <NewsCarousel
          handleOpen={handleOpen}
          columnSize={3}
          news={news}
        />
      )}
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Card className={classes.card}>
              <CardMedia className={classes.media} image={image || 'https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png'} title={openNews?.title} />
              <Typography className={classes.title} gutterBottom variant="h5" component="h2">{openNews?.title}</Typography>
              <CardContent>
                <Typography variant="body2" color="textSecondary" component="p">{openNews?.text}</Typography>
              </CardContent>
              {profile?.role === 'admin' && <CardActions className={classes.cardActions}>
                <Button size="small" color="primary" onClick={() => removeHandler(openNews?._id)}>
                  <DeleteIcon fontSize="small" /> Delete
                </Button>
                <Button size="small" color="primary" component={Link} to={"/change-news/" + openNews?._id}>
                  <SettingsIcon fontSize="small" /> Change
                </Button>
              </CardActions>}
            </Card>
          </Box>
        </Fade>
      </Modal>
    </Stack>
  )
};

export default NewsBlock;