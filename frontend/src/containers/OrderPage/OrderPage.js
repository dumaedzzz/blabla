import React, {useEffect, useState} from 'react';
import {Button, CircularProgress, Grid, MenuItem, TextField, Typography} from "@mui/material";
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {makeStyles} from "@mui/styles";
import {Link} from "react-router-dom";
import moment from 'moment';
import {useDispatch, useSelector} from "react-redux";
import {fetchOrdersRequest} from "../../store/actions/ordersActions";


const useStyles = makeStyles(theme => ({
  orderPageContainer: {
    margin: '40px auto 50px',
  },
  label: {
    textAlign: 'center',
    fontFamily: '\'Montserrat\', sans-serif',
    marginTop: '30px'
  },
  orderTable: {
    marginBottom: '50px'
  }
}))

const OrderPage = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {orders, fetchOrdersLoading} = useSelector(state => state.orders);



  const statuses = [
    {_id: 2, title: 'новый', text: 'new'},
    {_id: 3, title: 'Принятый', text: 'submitted'},
    {_id: 4, title: 'Выполненный', text: 'done'},
    {_id: 5, title: 'Отклоненный', text: 'rejected'}
  ];


  const [state, setState] = useState({
    status: ""
  });



  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

  useEffect(() => {
    dispatch(fetchOrdersRequest());
  }, [dispatch]);

  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  return fetchOrdersLoading ? (
    <Grid container justifyContent="center" alignItems="center">
      <Grid item>
        <CircularProgress/>
      </Grid>
    </Grid>
  ) : (
    <>
      <div className="container">
        <Typography variant="h4" className={classes.label}>Заказы</Typography>
        <div className={classes.orderPageContainer}>
          <Grid container justifyContent="space-between" alignItems="center" spacing={2}>
            <Grid item xs={12} sm={5} md={3}
            >
              <TextField
                type="text"
                select
                fullWidth
                required
                color={'primary'}
                label="Статус работы"
                name="status"
                value={state.status}
                onChange={inputChangeHandler}
              >
                {statuses.map(status => (
                  <MenuItem
                    key={status._id}
                    value={status._id}
                  >
                    {status.title}
                  </MenuItem>
                ))}
              </TextField>
            </Grid>
            <Grid item xs={12} sm={5} md={3}>
              <Button
                variant={"contained"}
                component={Link} to="/createOrder"
                fullWidth
              >
                Оформить заказ
              </Button>
            </Grid>
          </Grid>
        </div>
        <TableContainer component={Paper} className={classes.orderTable}>
          <Table sx={{ minWidth: 700 }} aria-label="customized table">
            <TableHead>
              <TableRow>
                <StyledTableCell>Заказ</StyledTableCell>
                <StyledTableCell align="right">Дата</StyledTableCell>
                <StyledTableCell align="right">Комментарий</StyledTableCell>
                <StyledTableCell align="right">Статус</StyledTableCell>
                <StyledTableCell align="right">Мастер</StyledTableCell>
                <StyledTableCell align="right">Автор</StyledTableCell>
                <StyledTableCell align="right" />
              </TableRow>
            </TableHead>
            <TableBody>
              {orders.map((row, i) => (
                <StyledTableRow key={i}>
                  <StyledTableCell component="th" scope="row">
                    {row.orderNumber}
                  </StyledTableCell>
                  <StyledTableCell align="right">{moment(row.date).lang("ru").format('L')}</StyledTableCell>
                  <StyledTableCell align="right">{row.description}</StyledTableCell>
                  <StyledTableCell align="right">{row.status}</StyledTableCell>
                  <StyledTableCell align="right">{row.workshop?.name}</StyledTableCell>
                  <StyledTableCell align="right">{row.customer?.name}</StyledTableCell>
                  <StyledTableCell align="right"><Button>Подробнее</Button></StyledTableCell>
                </StyledTableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </div>
    </>
  );
};

export default OrderPage;