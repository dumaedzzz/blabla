import React, {useEffect, useState} from 'react';
import {CircularProgress, Grid, TextField, Typography} from "@mui/material";
import ButtonWithProgress from "../../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {makeStyles} from "@mui/styles";
import {useDispatch, useSelector} from "react-redux";
import MenuItem from "@mui/material/MenuItem";
import {createOrderRequest} from "../../../store/actions/ordersActions";
import {fetchWorkshopsRequest} from "../../../store/actions/workshopActions";

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: theme.spacing(2),
    textAlign: 'center',
  },
  alert: {
    marginTop: theme.spacing(3),
    width: "100%"
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
    padding: '10px'
  },
  formContainer: {
    maxWidth: '500px',
    backgroundColor: '#FEFEFE',
    border: '1px solid #c1c1c1',
    borderRadius: '8px',
    padding: '30px',
    margin: '50px auto',
  },
  inputForm: {
    width: '400px',
  },
  label: {
    textAlign: 'center',
    fontFamily: '\'Montserrat\', sans-serif',
    marginTop: '150px',
  }
}));

const CreateOrder = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const {workshops} = useSelector(state => state.workshops);
  const {createOrderLoading} = useSelector(state => state.orders);
  const [state, setState] = useState({
    serialNo: "",
    description: "",
    workshop: "",
    repairer: "",
  });

  useEffect(() => {
    dispatch(fetchWorkshopsRequest());
  }, [dispatch]);

  const inputChangeHandler = e => {
    const name = e.target.name;
    const value = e.target.value;
    setState(prevState => {
      return {...prevState, [name]: value};
    });
  };

  const submitFormHandler = async e => {
    e.preventDefault();
    const workshop = workshops.filter(w => w.name === state.workshop);
    await dispatch(createOrderRequest({...state, workshop: workshop[0]}))
  };

  return createOrderLoading ? (
    <Grid container justifyContent="center" alignItems="center">
      <Grid item>
        <CircularProgress/>
      </Grid>
    </Grid>
  ) :(
    <div className="container">
      <Typography variant="h4" className={classes.label}>Оформить заказ</Typography>
      <div className={classes.formContainer}>
        <Grid
          container
          direction="column"
          spacing={2}
          component="form"
          className={classes.root}
          autoComplete="off"
          onSubmit={submitFormHandler}
          noValidate
        >
          <Grid item xs={12}>
            <TextField
              fullWidth
              required
              color={'primary'}
              type="text"
              label="Серийный номер"
              name="serialNo"
              value={state.serialNo}
              onChange={inputChangeHandler}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              select
              label="Мастерской"
              fullWidth
              color={'action1'}
              onChange={inputChangeHandler}
              name="workshop"
              value={state.workshop}
            >
              {workshops.map((option) => (
                <MenuItem key={option._id} value={option.name}>
                  {option.name}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              multiline
              rows={4}
              color={'primary'}
              type="text"
              label="Комментарий"
              name="description"
              value={state.description}
              onChange={inputChangeHandler}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              fullWidth
              color={'primary'}
              type="text"
              label="Имя мастера если у вы уже знаете"
              name="warehouseman"
              value={state.warehouseman}
              onChange={inputChangeHandler}
            />
          </Grid>
          <Grid item xs={12}>
            <ButtonWithProgress
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              loading={createOrderLoading}
              disabled={createOrderLoading}
            >
              Оформить заказ
            </ButtonWithProgress>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default CreateOrder;