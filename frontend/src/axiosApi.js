import axios from 'axios';
import {BASE_URL} from "./config";
import store from "./store/configurateStore";

const axiosApi = axios.create({
  baseURL: BASE_URL,
});

axiosApi.interceptors.request.use(config => {
  const {users} = store.getState();
  if (users?.profile?.token){
    config.headers = {
      Authorization: users.profile.token,
    }
  }
  return config;
}, error => {
  return Promise.reject({error});
});

export default axiosApi;