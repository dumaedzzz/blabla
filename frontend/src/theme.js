import {createTheme} from "@mui/material/styles";

const mainBgColor = '#FBFBFB';

const theme = createTheme({
  palette: {
    primary: {
      main: '#4D77E3',
    },
    action1: {
      main: '#122c86',
      contrastText: 'white',
    },
  },
  components: {
    MuiTextField: {
      styleOverrides: {
        root: {
          backgroundColor: 'white',
          "& .MuiFormHelperText-root": {
            backgroundColor: mainBgColor,
            padding: '0 10px',
            margin: 0,
          },
        },
      },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: 'none',
        },
      },
    },
  },
});

theme.typography.h4 = {
  fontSize: '2.125rem',
  fontWeight: 300,
  [theme.breakpoints.down('sm')]: {
    fontSize: '1.5rem',
  },
}

export default theme;