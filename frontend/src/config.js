export let BASE_URL = 'http://localhost:8000';
export const USERS_URL = '/users';
export const USERS_LOG_IN_URL = '/users/sessions';
export const EMPLOYEES_LOG_IN_URL = '/employees/sessions';

if (process.env.REACT_APP_ENV === 'test') {
  BASE_URL = 'http://localhost:8010';
}