import store from "./store/configurateStore";
import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import {ToastContainer} from "react-toastify";
import history from "./History";
import App from './App';
import {BrowserRouter} from "react-router-dom";
import 'react-toastify/dist/ReactToastify.css';
import {StyledEngineProvider, ThemeProvider} from '@mui/material/styles';
import NavigateSetter from "./components/NavigateSetter/NavigateSetter";
import theme from "./theme";
import './index.css';


ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <StyledEngineProvider injectFirst>
        <BrowserRouter history={history}>
          <NavigateSetter/>
          <ToastContainer
            position={"bottom-right"}
          />
          <App/>
        </BrowserRouter>
      </StyledEngineProvider>
    </ThemeProvider>
  </Provider>
  , document.getElementById('root')
);

