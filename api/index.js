const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');
const users = require('./routers/users/users');
const employees = require('./routers/employees/employees');
const warehouses = require('./routers/warehouses/warehouses');
const workshops = require('./routers/workshops/workshops');
const news = require('./routers/news/news');
const typeDetails = require('./routers/typeDetails/typeDetails');
const partNumbersDetails = require('./routers/partNumbersDetails/partNumbersDetails');
const detailsInTheWarehouses = require('./routers/detailsInTheWarehouse/detailsInTheWarehouse');
const accountsDetails = require('./routers/accountsDetails/accountsDetails');
const details = require('./routers/details/details');
const orders = require('./routers/orders/orders');


const server = express();
server.use(express.json());
server.use(express.static('public'));
server.use(cors());

server.use('/users', users);
server.use('/employees', employees);
server.use('/warehouses', warehouses);
server.use('/workshops', workshops);
server.use('/news', news);
server.use('/typeDetails', typeDetails);
server.use('/partNumbersDetails', partNumbersDetails);
server.use('/detailsInTheWarehouses', detailsInTheWarehouses);
server.use('/accountsDetails', accountsDetails);
server.use('/details', details);
server.use('/orders', orders);

server.get('/', (req, res) => {
    res.send('Hello team')
})

const run = async () => {
  await mongoose.connect(config.db.url);

  server.listen(config.port, () => {
    console.log(`Server is started on ${config.port} port !`);
  })

  exitHook(() => {
    console.log('exiting');
    mongoose.disconnect();
  });
}
run().catch(e => console.error(e));