const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const validateUnique = async value => {
	const detail = await PartNumberDetail.findOne({partNumber: value});
	if (detail) return false;
};

const PartNumberDetailSchema = new mongoose.Schema({
	type: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'TypeDetail',
		required: true,
	},
	partNumber: {
		type: String,
		required: true,
		unique: true,
		validate: [
			{validator: validateUnique, message: 'Данный партномер уже используется!'},
		],
	},
	sizeIn: {
		type: String,
	},
	sizeOut: {
		type: String,
	},
	material: {
		type: String,
	},
});

PartNumberDetailSchema.plugin(idValidator);

const PartNumberDetail = mongoose.model('PartNumberDetail', PartNumberDetailSchema);
module.exports = PartNumberDetail;