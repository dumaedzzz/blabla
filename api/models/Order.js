const mongoose = require('mongoose');
const {nanoid} = require("nanoid");

const OrderSchema = new mongoose.Schema({
  orderNumber: {
    type: String,
    default: nanoid(6),
  },
  date: {
    type: Date,
    default: new Date(),
  },
  serialNo: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  workshop: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Workshop',
    required: true,
  },
  repairer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Employee',
  },
  status: {
    type: String,
    required: true,
    enum: ['Новый', 'Принято', "Отклонено", "Сделано"],
    default: 'Новый',
  },
  customer: {
    type: mongoose.Schema.Types.Mixed,
    required: true,
  }
});

const Order = mongoose.model('Order', OrderSchema);
module.exports = Order;