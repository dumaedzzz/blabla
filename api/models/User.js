const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const {nanoid} = require('nanoid');

const SALT_WORK_FACTOR = 10;

const validateUnique = async value => {
  const user = await User.findOne({email: value});
  if (user) return false;
};

const validateEmail = value => {
  const re = /^(\w+[-.]?\w+)@(\w+)([.-]?\w+)?(\.[a-zA-Z]{2,})$/;
  if (!re.test(value)) return false;
};

const validatePhoneNumber = value => {
  const re = /^[+]*[(]?[0-9]{1,4}[)]/;
  if (!re.test(value)) return false;
};

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    validate: [
      {validator: validateEmail, message: 'Email is not valid!'},
      {validator: validateUnique, message: 'This user is already registered!'},
    ],
  },
  password: {
    type: String,
    required: true
  },
  token: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  phone: {
    type: String,
    required: true,
    validate: {validator: validatePhoneNumber, message: 'Phone number is not valid'},
  },
  address: {
    type: String,
    required: true,
  },
  avatar: String,
  status: {
    type: String,
    required: true,
    default: 'active',
    enum: ['active', 'inactive'],
  },
});

UserSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();

  const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
  this.password = await bcrypt.hash(this.password, salt);

  next();
});

UserSchema.set('toJSON', {
  transform: (doc, ret) => {
    delete ret.password;
    return ret;
  },
});

UserSchema.methods.checkPassword = function (password) {
  return bcrypt.compare(password, this.password);
};

UserSchema.methods.generateToken = function () {
  this.token = nanoid();
};

const User = mongoose.model('User', UserSchema);

module.exports = User;