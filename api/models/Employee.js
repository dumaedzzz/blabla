const mongoose = require('mongoose');
const {nanoid} = require("nanoid");
const bcrypt = require('bcrypt');
const Workshop = require('./Workshop');

const SALT_WORK_FACTOR = 10;

const validateEmailUnique = async value => {
  const employee = await Employee.findOne({email: value});
  if (employee) return false;
};

const validateEmail = value => {
  const re = /^(\w+[-.]?\w+)@(\w+)([.-]?\w+)?(\.[a-zA-Z]{2,})$/;
  if (!re.test(value)) return false;
};

const validatePhoneNumberUnique = async value => {
  const employee = await Employee.findOne({phoneNumber: value});
  if (employee) return false;
};

const validatePhoneNumber = value => {
  const re = /^[0][(][0-9]{2,3}[)]([-][0-9]{2}){3}$/;
  if (!re.test(value)) return false;
};

const validateINN = value => {
  if (value.length !== 14) return false;
};

const validateINNUnique = async value => {
  const employee = await Employee.findOne({INN: value});
  if (employee) return false;
};

const checkWorkshopStatus = async value => {
  const workshop = await Workshop.findById(value);
  if (!workshop.status) return false;
};

const EmployeeSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
    validate: [
      {validator: validateEmail, message: 'Не правильный формат почты!'},
      {validator: validateEmailUnique, message: 'Данный клиент уже зарегистрирован!'}
    ],
  },
  password: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  INN: {
    type: String,
    unique: true,
    required: true,
    validate: [
      {validator: validateINN, message: 'ИНН должен состоять из 14 цифр!'},
      {validator: validateINNUnique, message: 'Данный ИНН уже используется!'},
    ],
  },
  phoneNumber: {
    type: String,
    required: true,
    unique: true,
    validate: [
      {validator: validatePhoneNumberUnique, message: 'Данный номер уже используется другим пользователем!'},
      {validator: validatePhoneNumber, message: 'Введите правильный формат номера телефона 0(XXX)-XX-XX-XX'}
    ],
  },
  address: String,
  avatar: String,
  role: {
    type: String,
    enum: ['admin','repairer','warehouseman'],
    required: true,
  },
	workshop: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Workshop',
    validate: [
      {validator: checkWorkshopStatus, message: 'Данная мастерская неактивна!'},
    ],
  },
  warehouse: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Warehouse',
  }],
  status: {
    type: Boolean,
    required: true,
    default: true,
  },
  token: {
    type: String,
    required: true,
  },
});

EmployeeSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next();

  const salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
  const hash = await bcrypt.hash(this.password, salt);

  this.password = hash;

  next();
});

EmployeeSchema.set('toJSON', {
  transform: (doc, ret, options) => {
    delete ret.password;
    return ret;
  },
});

EmployeeSchema.methods.checkPassword = function (password) {
  return bcrypt.compare(password, this.password);
};

EmployeeSchema.methods.generateToken = function () {
  this.token = nanoid();
};

const Employee = mongoose.model('Employee', EmployeeSchema);

module.exports = Employee;