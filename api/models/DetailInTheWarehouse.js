const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const DetailInTheWarehouseSchema = new mongoose.Schema({
	warehouse: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Warehouse',
		required: true,
	},
	type: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'TypeDetail',
		required: true,
	},
	partNumber: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'PartNumberDetail',
		required: true,
	},
	sizeIn: {
		type: String,
	},
	sizeOut: {
		type: String,
	},
	material: {
		type: String,
	},
	quantity: {
		type: Number,
	},
	completedPrice: {
		type: String,
	},
});

DetailInTheWarehouseSchema.plugin(idValidator);
const DetailInTheWarehouse = mongoose.model('DetailInTheWarehouse', DetailInTheWarehouseSchema);
module.exports = DetailInTheWarehouse;