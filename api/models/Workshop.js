const mongoose = require('mongoose');

const validateNameUnique = async value => {
  const workshop = await Workshop.findOne({name: value});
  if (workshop) return false;
};

const validateWarehouseUnique = async value => {
  const workshop = await Workshop.findOne({warehouse: value});
  if (workshop) return false;
};

const WorkshopSchema = new mongoose.Schema({
  warehouse:{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Warehouse',
    required: true,
    validate: [
      {validator: validateWarehouseUnique, message: 'Данный склад используется другой мастерской'},
    ],
  },
  name: {
    type: String,
    required: true,
    validate: [
      {validator: validateNameUnique, message: 'Данное название склада уже используется!'},
    ],
  },
  status: {
    type: Boolean,
    required: true,
    default: true,
  }
});

const Workshop = mongoose.model('Workshop', WorkshopSchema);

module.exports = Workshop;