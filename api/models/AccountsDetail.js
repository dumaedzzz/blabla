const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const DetailsSchema = new mongoose.Schema({
	type: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'TypeDetail',
		required: true,
	},
	partNumber: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'PartNumberDetail',
		required: true,
	},
	quantity: {
		type: String,
		required: true,
	},
  priceInitial: {
		type: String,
	  required: true,
	},
});

const AccountsDetailSchema = new mongoose.Schema({
	fromWarehouse: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Warehouse',
	},
	toWarehouse: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Warehouse',
	},
	employee: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Employee',
	},
	date: {
		type: Date,
		required: true,
	},
	details: {
		type: [DetailsSchema],
	},
	deliveryPrice: String,
	commissionPrice: String,
	customPrice: String,
	totalPrice: String,
	title: String,
	description: String,
	typeOfAction: {
		type: String,
		required: true,
		enum: ['expense', 'income', 'migration'],
	}
});

AccountsDetailSchema.plugin(idValidator);
const AccountsDetail = mongoose.model('AccountDetail', AccountsDetailSchema);
module.exports = AccountsDetail;