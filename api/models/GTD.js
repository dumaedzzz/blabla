const mongoose = require('mongoose');

const GTDSchema = new mongoose.Schema({
  auto: {
    type: String,
    required: true,
  },
  model: {
    type: String,
    required: true,
  },
  year: {
    type: Number,
    required: true,
  },
  value: {
    type: String,
    required: true,
  },
  transmission: {
    type: String,
    required: true,
  },
  serialNo: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  }
});

const GTD = mongoose.model('GTD', GTDSchema);
module.exports = GTD;