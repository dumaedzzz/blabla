const mongoose = require('mongoose');

const validateUnique = async value => {
	const detail = await TypeDetail.findOne({type: value});
	if (detail) return false;
};

const TypeDetailSchema = new mongoose.Schema({
	type: {
		type: String,
		required: true,
		unique: true,
		validate: [
			{validator: validateUnique, message: 'Данный тип уже используется!'},
		],
	},
});

const TypeDetail = mongoose.model('TypeDetail', TypeDetailSchema);
module.exports = TypeDetail;