const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const Employee = require("./models/Employee");
const User = require("./models/User");
const Warehouse = require("./models/Warehouse");
const Workshop = require("./models/Workshop");
const News = require("./models/News");
const GTD = require("./models/GTD");
const Order = require("./models/Order");
const config = require("./config");
const TypeDetail = require("./models/TypeDetail");
const PartNumberDetail = require("./models/PartNumberDetail");

const run = async () => {
  await mongoose.connect(config.db.url)

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [firstUser, secondUser] = await User.create(
    {
      email: 'user@gmail.com',
      password: '123',
      token: nanoid(),
      name: 'SimpleDummy',
      phone: '+(996)500-50-50-50',
      address: "Bishkek",
    },
    {
      email: 'user2@gmail.com',
      password: '123',
      token: nanoid(),
      name: 'SimpleDummy2',
      phone: '+(996)500-50-50-51',
      address: "Bishkek",
      status: "inactive",
    },
  );

  const [first, second, third, fourth, fifth, sixth] = await Employee.create(
    {
      email: 'admin@gmail.com',
      password: '123',
      name: 'John',
      phoneNumber: '0(500)-50-50-56',
      address: "Bishkek",
      INN: '22345678901234',
      role: 'admin',
      token: nanoid(),
    },
    {
      email: 'repairer@gmail.com',
      password: '123',
      name: 'John',
      phoneNumber: '0(500)-50-50-55',
      address: "Bishkek",
      INN: '22345678901235',
      role: 'repairer',
      token: nanoid(),
    },
    {
      email: 'repairer1@gmail.com',
      password: '123',
      name: 'John',
      phoneNumber: '0(500)-50-50-54',
      address: "Bishkek",
      INN: '22345678901236',
      role: 'repairer',
      token: nanoid(),
      status: false,
    },
    {
      email: 'warehouseman@gmail.com',
      password: '123',
      name: 'John',
      phoneNumber: '0(500)-50-50-53',
      address: "Bishkek",
      INN: '22345678901237',
      role: 'warehouseman',
      token: nanoid(),
      status: false,
    },
    {
      email: 'warehouseman1@gmail.com',
      password: '123',
      name: 'John',
      phoneNumber: '0(500)-50-50-52',
      address: "Bishkek",
      INN: '22345678901238',
      role: 'warehouseman',
      token: nanoid(),
    },
  );

  const [warehouseFirst, warehouseSecond, warehouseThird] = await Warehouse.create(
    {
      name: 'Склад-1',
      status: true
    }, {
      name: 'Склад-2',
      status: false
    }, {
      name: 'Склад-3',
      status: true
    }
  );


  const [firstW, secondW, thirdW,] = await Workshop.create(
    {
      warehouse: warehouseFirst._id,
      name: "Мастерская 1",
    },
    {
      warehouse: warehouseSecond._id,
      name: "Мастерская 2",
    },
    {
      warehouse: warehouseThird._id,
      name: "Мастерская 3",
  }
  );

  await GTD.create(
    {
      auto: 'Lexus',
      model: 'LS460',
      year: 2004,
      value: '3,5',
      transmission: 'avtomat',
      serialNo: 'asd123',
      description: 'text',
    },
    {
      auto: 'Toyota',
      model: 'Land Cruiser 200',
      year: 2012,
      value: '3,5',
      transmission: 'mechanik',
      serialNo: 'asd123',
      description: 'text',
    }
  );

  await Order.create(
    {
      serialNo: 'asd123',
      description: 'text',
      workshop: firstW,
      customer: secondUser
    },
    {
      serialNo: 'asd123',
      description: 'text text',
      workshop: secondW,
      customer: firstUser,
    }
  )


  await News.create(
    {
      title: "News 1",
      text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab at dolor iste maxime quae, quia quibusdam repellendus repudiandae sint tenetur.",
      image: 'fixtures/cpu.jpg'
    },{
      title: "News 2",
      text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab at dolor iste maxime quae, quia quibusdam repellendus repudiandae sint tenetur.",
      image: 'fixtures/cpu.jpg',
    },
    {
      title: "News 3",
      text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab at dolor iste maxime quae, quia quibusdam repellendus repudiandae sint tenetur.",
      image: 'fixtures/hdd.jpg'
    },
    {
      title: "News 4",
      text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab at dolor iste maxime quae, quia quibusdam repellendus repudiandae sint tenetur.",
      image: 'fixtures/hdd.jpg'
    },
  )

  const [type1, type2, type3] = await TypeDetail.create(
    {type: 'Гвоздь'},
    {type: 'Болт'},
    {type: 'Шуруп'},
  );

  await PartNumberDetail.create(
    {type: type1, partNumber: 'a1', sizeIn: '10mm', sizeOut: '20mm', material: 'iron'},
    {type: type1, partNumber: 'a2', sizeIn: '5mm', sizeOut: '8mm', material: 'iron'},
    {type: type2, partNumber: 'b1', sizeIn: '5mm', sizeOut: '8mm', material: 'iron'},
    {type: type2, partNumber: 'b3', sizeIn: '5mm', sizeOut: '2mm', material: 'iron'},
    {type: type3, partNumber: 'c4', sizeIn: '1mm', sizeOut: '3mm', material: 'iron'},
  );

  await mongoose.connection.close();
};

run().catch(console.error);

