const express = require('express');
const Warehouse = require('../../models/Warehouse');
const permit = require("../../middleware/permit");
const authForEmployee = require('../../middleware/authForEmployee');

const router = express.Router();

router.get('/', authForEmployee, async (req, res) => {
  try {
    if (req.query.warehouse){
      const warehouse = await Warehouse.findById(req.query.warehouse);
      res.send(warehouse);
    } else {
      const warehouses= await Warehouse.find();
      res.send(warehouses);
    }

  } catch (e){
    res.sendStatus(500);
  }
});

router.post('/new', authForEmployee, permit('admin'), async (req, res) => {
  const warehouseData = {
    name: req.body.name
  };

  const warehouse = new Warehouse(warehouseData);

  try {
    await warehouse.save();
    res.send(warehouse);
  } catch (error){
    res.status(400).send(error);
  }
});


router.put('/changeName/:id', authForEmployee, permit('admin'), async (req, res) => {
  const warehouse = await Warehouse.findById(req.params.id);

  if(warehouse) {
    warehouse.name = req.body.name;

    try {
      await warehouse.save();
      res.send(warehouse);
    } catch (error) {
      res.status(400).send(error);
    }
  } else {
    res.status(404).send({error: 'Склад не найден'});
  }
});

router.put('/changeStatus/:id', authForEmployee, permit('admin'), async (req, res) => {

  const warehouse = await Warehouse.findById(req.params.id);

  if(warehouse) {
    warehouse.status = !warehouse.status;

    try {
      await Warehouse.findByIdAndUpdate(req.params.id, warehouse, {new: true});
      return res.send(warehouse);
    } catch (error) {
      res.status(400).send(error);
    }
  } else {
    res.status(404).send({error: 'Склад не найден'});
  }

});

module.exports = router;

