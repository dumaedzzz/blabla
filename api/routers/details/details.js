const express = require('express');
const permit = require("../../middleware/permit");
const authForEmployee = require('../../middleware/authForEmployee');
const AccountsDetail = require('../../models/AccountsDetail');
const PartNumberDetail = require("../../models/PartNumberDetail");
const DetailInTheWarehouse = require("../../models/DetailInTheWarehouse");

const router = express.Router();

router.post('/', authForEmployee, permit('admin', 'warehouseman'), async (req, res) => {
	try {
		if (req.query.warehouse) {

			const arrDetails = JSON.parse(req.body.details);

			const data = {
				employee: req.user._id,
				fromWarehouse: req.body.fromWarehouse || null,
				toWarehouse: req.body.toWarehouse || null,
				date: new Date().toISOString(),
				details: JSON.parse(req.body.details),
				deliveryPrice: req.body.deliveryPrice,
				commissionPrice: req.body.commissionPrice,
				customPrice: req.body.customPrice,
				totalPrice: req.body.totalPrice,
				title: req.body.title || '',
				description: req.body.description || '',
				typeOfAction: req.body.typeOfAction,
			};

			for (let d = 0; d < arrDetails.length; d++) {
				const findByPartNumberId = await PartNumberDetail.findById(arrDetails[d].partNumber);

				const detailData = {
					warehouse: req.query.warehouse,
					type: arrDetails[d].type,
					partNumber: arrDetails[d].partNumber,
					sizeIn: findByPartNumberId.sizeIn,
					sizeOut: findByPartNumberId.sizeOut,
					material: findByPartNumberId.material,
					quantity: arrDetails[d].quantity,
				};

				const hasPartNumber = await DetailInTheWarehouse.findOne({partNumber: arrDetails[d].partNumber});

				if (hasPartNumber) {
					if (arrDetails[d].typeOfAction === 'income') {
						await DetailInTheWarehouse.findOneAndUpdate(
							{partNumber: arrDetails[d].partNumber},
							{$inc: {quantity: arrDetails[d].quantity}},
							{new: true}
						);
					}


					if (arrDetails[d].typeOfAction === 'expense') {
						await DetailInTheWarehouse.findOneAndUpdate(
							{partNumber: arrDetails[d].partNumber},
							{$inc: {quantity: -arrDetails[d].quantity}},
							{new: true}
						);
					}
				} else {
					const detail = new DetailInTheWarehouse(detailData);
					await detail.save();
				}
			}

			const newData = new AccountsDetail(data);
			await newData.save();
			return res.send({message: 'Success!'});
		}
	} catch (error) {
		res.status(400).send(error);
	}
});

module.exports = router;
