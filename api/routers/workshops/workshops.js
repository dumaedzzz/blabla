const express = require('express');
const Workshop = require('../../models/Workshop');
const authForEmployee = require("../../middleware/authForEmployee");
const permit = require("../../middleware/permit");

const router = express.Router();

router.get('/', authForEmployee, async (req, res) => {
  try {

    if (req.query.workshop){
      const workshop = await Workshop.findById(req.query.workshop);
      res.send(workshop);
    } else {
      const workshops = await Workshop.find();
      res.send(workshops);
    }

  } catch (e){
    res.sendStatus(500);
  }
});

router.post('/new', authForEmployee, permit('admin'), async (req, res) => {
  const workshopData = {
    warehouse: req.body.warehouse,
    name: req.body.name
  };

  const workshop = new Workshop(workshopData);

  try {
    await workshop.save();
    res.send(workshop);
  } catch (error){
    res.status(400).send(error);
  }
});


router.put('/changeName/:id', authForEmployee, permit('admin'), async (req, res) => {
  const workshop = await Workshop.findById(req.params.id);
  const anotherWorkshop = await Workshop.findOne({name: req.body.name});

  if(workshop) {

    if (anotherWorkshop){
      res.status(400).send({error: 'Данное имя используется другой мастерской'});
    }

    delete workshop.warehouse;
    workshop.name = req.body.name;

    try {
      await Workshop.findByIdAndUpdate(req.params.id, workshop, {new: true});
      return res.send(workshop);
    } catch (error) {
      res.status(400).send(error);
    }

  } else {
    res.status(404).send({error: 'Мастерская не найдена'});
  }
});

router.put('/changeStatus/:id', authForEmployee, permit('admin'), async (req, res) => {

  const workshop = await Workshop.findById(req.params.id);

  if(workshop) {
    workshop.status = !workshop.status;

    try {
      await Workshop.findByIdAndUpdate(req.params.id, workshop, {new: true});
      return res.send(workshop);
    } catch (error) {
      res.status(400).send(error);
    }
  } else {
    res.status(404).send({error: 'Мастерская не найдена'});
  }

});



module.exports = router;