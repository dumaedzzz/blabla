const express = require('express');
const router = express.Router();
const Order = require('../../models/Order');
const permit = require("../../middleware/permit");
const authForEmployee = require("../../middleware/authForEmployee");


router.get('/',async(req, res) => {
  try{
    const orders = await Order.find().populate('workshop', 'name');
    res.send(orders);
  }catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', authForEmployee, async(req, res) => {
  try{
    if(!req.body.serialNo) return res.status(401).send({message: 'Поля для серыйный номер не заполнено!'});
    if(!req.body.workshop) return res.status(401).send({message: 'Поля для мастерских не заполнено!'});
    if(!req.body.description) return res.status(401).send({message: 'Поля для описание не заполнено!'});

    let orderData = {
      serialNo: req.body.serialNo,
      workshop: req.body.workshop,
      description: req.body.description,
      customer: req.user,
    };

    if(req.body.repairer){
      orderData.repairer = req.body.warehouseman;
    }

    const order = new Order(orderData);

    await order.save();
    res.send(order);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.put('/:id', authForEmployee,async(req, res) => {
  try{
    const {id} = req.params;
    const order = await Order.findById(id);
    if(!req.body.serialNo) return res.status(401).send({message: 'Поля для серыйный номер не заполнено!'});
    if(!req.body.workshop) return res.status(401).send({message: 'Поля для мастерских не заполнено!'});
    if(!req.body.description) return res.status(401).send({message: 'Поля для описание не заполнено!'});

    let orderData = {
      serialNo: req.body.serialNo,
      workshop: req.body.workshop,
      repairer: req.body.repairer,
      description: req.body.description,
      _id: id
    };
    if(!order){
      return res.status(404).send({message: 'Заказ не найдено!'});
    }
    await Order.findByIdAndUpdate(id, orderData, {new: true});
    return res.send(orderData);
  }catch (e) {
    res.sendStatus(500);
  }
});

router.put('/changeStatus/:id', authForEmployee,  permit('admin'),async(req, res) => {
  try{
    const {id} = req.params;
    const order = await Order.findById(id);
    if(!req.body.status) return res.status(401).send({message: 'Поля для  статус не заполнено!'});

    if(!order){
      return res.status(404).send({message: 'Заказ не найдено!'});
    }
    order.status = req.body.status;
    await Order.findByIdAndUpdate(id, order, {new: true});
    return res.send(order);
  }catch (e) {
    res.sendStatus(500);
  }
});

router.delete('/:id', authForEmployee, permit('admin'), async(req, res) => {
  try {
    const order = await Order.findByIdAndDelete(req.params.id);

    if (order) {
      res.send(`Заказ удалено!`);
    } else {
      res.status(404).send({message: 'Заказ не найдено!'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});


module.exports = router;
