const express = require('express');
const authForEmployee = require('../../middleware/authForEmployee');
const DetailInTheWarehouse = require("../../models/DetailInTheWarehouse");

const router = express.Router();

router.get('/', authForEmployee, async (req, res) => {
	try {
		if (req.query.warehouse) {
			const warehouseDetails = await DetailInTheWarehouse.find({warehouse: req.query.warehouse});
			res.send(warehouseDetails);
		} else {
			const details = await DetailInTheWarehouse.find();
			res.send(details);
		}
	} catch (error) {
		res.sendStatus(500);
	}
});

module.exports = router;