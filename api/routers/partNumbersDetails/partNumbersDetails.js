const express = require('express');
const permit = require("../../middleware/permit");
const authForEmployee = require('../../middleware/authForEmployee');
const PartNumberDetail = require('../../models/PartNumberDetail');

const router = express.Router();

router.get('/', authForEmployee, async (req, res) => {
	try {
		if (req.query.partNumberID) {
			const partNumberID = await PartNumberDetail.findById(req.query.partNumberID);

			if (!partNumberID) {
				return res.status(404).send({error: 'Партномер не найден'});
			}

			return res.send(partNumberID);
		}

		if (req.query.partNumber) {
			const partNumber = await PartNumberDetail.findOne({partNumber: req.query.partNumber});

			if (!partNumber) {
				return res.status(404).send({error: 'Партномер не найден'});
			}

			return res.send(partNumber);
		}

		const partNumbers = await PartNumberDetail.find();
		res.send(partNumbers);
	} catch (error) {
		res.sendStatus(500);
	}
});

router.post('/', authForEmployee, permit('admin', 'warehouseman'), async (req, res) => {
	try {
		const partNumberData = {
			type: req.body.type,
			partNumber: req.body.partNumber,
			sizeIn: req.body.sizeIn,
			sizeOut: req.body.sizeOut,
			material: req.body.material,
		};

		const type = new PartNumberDetail(partNumberData);
		await type.save();
		res.send(type);
	} catch (error) {
		res.status(400).send(error);
	}
});

module.exports = router;