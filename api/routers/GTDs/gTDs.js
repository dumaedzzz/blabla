const express = require('express');
const router = express.Router();
const GTD = require('../../models/GTD');
const permit = require("../../middleware/permit");
const authForEmployee = require("../../middleware/authForEmployee");


router.get('/',async(req, res) => {
  try{
    const gTDS = await GTD.find();
    res.send(gTDS);
  }catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', authForEmployee,  permit('admin'), async(req, res) => {
  try{
    if(!req.body.auto) return res.status(401).send({message: 'Поля для авто заполнено!'});
    if(!req.body.model) return res.status(401).send({message: 'Поля для модель не заполнено!'});
    if(!req.body.year) return res.status(401).send({message: 'Год авто не заполнено!'});
    if(!req.body.count) return res.status(401).send({message: 'Количество не заполнено!'});
    if(!req.body.transmission) return res.status(401).send({message: 'Коробка передач не заполнено!'});
    if(!req.body.serialNo) return res.status(401).send({message: 'Серийный номер не заполнено!'});
    if(!req.body.description) return res.status(401).send({message: 'Описание не заполнено!'});

    let gTD_data = {
      auto: req.body.auto,
      model: req.body.model,
      year: req.body.year,
      count: req.body.count,
      transmission: req.body.transmission,
      serialNo: req.body.serialNo,
      description: req.body.description,
    };


    const gTD = new GTD(gTD_data);

    await gTD.save();
    res.send(gTD);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.put('/:id', authForEmployee,  permit('admin'),async(req, res) => {
  try{
    const {id} = req.params;
    const gTD = await GTD.findById(id);
    if(!req.body.auto) return res.status(401).send({message: 'Поля для авто заполнено!'});
    if(!req.body.model) return res.status(401).send({message: 'Поля для модель не заполнено!'});
    if(!req.body.year) return res.status(401).send({message: 'Год авто не заполнено!'});
    if(!req.body.count) return res.status(401).send({message: 'Количество не заполнено!'});
    if(!req.body.transmission) return res.status(401).send({message: 'Коробка передач не заполнено!'});
    if(!req.body.serialNo) return res.status(401).send({message: 'Серийный номер не заполнено!'});
    if(!req.body.description) return res.status(401).send({message: 'Описание не заполнено!'});

    let gTD_data = {
      auto: req.body.auto,
      model: req.body.model,
      year: req.body.year,
      count: req.body.count,
      transmission: req.body.transmission,
      serialNo: req.body.serialNo,
      description: req.body.description,
      _id: id,
    };
    if(!gTD){
      return res.status(404).send({message: 'ГТФ не найдено!'});
    }
    await GTD.findByIdAndUpdate(id, gTD_data, {new: true});
    return res.send(gTD_data);
  }catch (e) {
    res.sendStatus(500);
  }
});

router.delete('/:id', authForEmployee, permit('admin'), async(req, res) => {
  try {
    const gTD = await GTD.findByIdAndDelete(req.params.id);

    if (gTD) {
      res.send(`ГТФ удалено!`);
    } else {
      res.status(404).send({message: 'ГТФ не найдено!'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});


module.exports = router;
