const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const Employee = require('../../models/Employee');
const Workshop = require('../../models/Workshop');
const config = require('../../config');
const permit = require("../../middleware/permit");
const authForEmployee = require('../../middleware/authForEmployee');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', authForEmployee, async (req, res) => {
  try {

    if (req.query.employee){
          const employee = await Employee.findById(req.query.employee);
          res.send(employee);
    } else if (req.query.role){

        if (req.query.role === 'repairer'){
          const employees = await Employee.find({role: 'repairer'});
          res.send(employees);
        } else if (req.query.role === 'warehouseman'){
          const employees = await Employee.find({role: 'warehouseman'});
          res.send(employees);
        }

    } else if (req.query.workshop){
      const employees = await Employee.find({workshop: req.query.workshop});
      res.send(employees);
    } else {
        const employees= await Employee.find().populate('warehouse');
        res.send(employees);
    }

  } catch (e){
    res.sendStatus(500);
  }
});

router.post('/new', authForEmployee, permit('admin'), upload.single('avatar'), async (req, res) => {
  const employeeData = {
    email: req.body.email,
    password: req.body.password,
    name: req.body.name,
    INN: req.body.INN,
    phoneNumber: req.body.phoneNumber,
    address: req.body.address,
    role: req.body.role,
    warehouse: [],
  };

  if (req.file) {
    employeeData.avatar = 'uploads/' + req.file.filename;
  } else {
    employeeData.avatar = null;
  }

  if (req.body.workshop){
    employeeData.workshop = req.body.workshop;
  }

  if (req.body.warehouse) {
    employeeData.warehouse.push(req.body.warehouse);
  }

  const employee = new Employee(employeeData);

  try {
    employee.generateToken();
    await employee.save();
    res.send(employee);
  } catch (error){
    res.status(400).send(error);
  }
});

router.post('/sessions', async (req, res) => {
  const employee = await Employee.findOne({email: req.body.email});

  const errMessage = {
    message: 'Пароль или электронная почта неверны'
  }
  try {
    if (!employee){
      return res.status(401).send({errors: errMessage});
    }

    const isMatch = await employee.checkPassword(req.body.password);

    if (!isMatch) {
      return res.status(401).send({errors: errMessage});
    }
    if (employee.status === 'inactive') return res.status(401).send({global: 'Ваш профиль деактивирован'});

    employee.generateToken();
    await employee.save({validateBeforeSave: false});
    res.send(employee);

  } catch (error){
    res.status(400).send(error);
  }
});

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Success'};

  if (!token) return res.send(success);

  const employee = await Employee.findOne({token});

  if (!employee) return res.send(success);

  employee.generateToken();
  await employee.save({validateBeforeSave: false});
  return res.send(success);
});

router.put('/', authForEmployee, upload.single('avatar'), async (req, res) => {
    const user = req.user.role;

    const newData = {
	    email: req.body.email,
	    password: req.body.password,
	    name: req.body.name,
	    INN: req.body.INN,
	    phoneNumber: req.body.phoneNumber,
	    address: req.body.address,
	    role: req.body.role,
    };

		if (req.file) {
			newData.avatar = 'uploads/' + req.file.filename;
		} else {
			newData.avatar = null;
		}

  if (req.body.workshop){

    try{
      const workshop = await  Workshop.findById(req.body.workshop);

      if (!workshop) return res.status(404).send({error: 'Мастерская не найдена'});

      if (!workshop.status){
        res.status(400).send({error: 'Данная мастерская не активна'});
      }

      newData.workshop = req.body.workshop;

    } catch (error){
      res.status(400).send(error);
    }

  }

    try{

      if (req.query.employee) {

          if (user.role === 'admin'){
            await Employee.findByIdAndUpdate(req.query.employee, newData, {new: true});
            return res.send('Изменения успешно сохранены');
          } else {
            delete newData.INN;
            await Employee.findByIdAndUpdate(req.query.employee, newData, {new: true});
            return res.send('Изменения успешно сохранены');
          }

      } else {
          res.status(404).send({error: 'Сотрудник не найден'});
      }
    } catch (error){
      res.status(400).send(error);
    }

});

router.put('/changeStatus/:id', authForEmployee, permit('admin'), async (req, res) => {
  const employee = await Employee.findById(req.params.id);

  if(employee) {
    employee.status = !employee.status;

    try {
      await Employee.findByIdAndUpdate(req.params.id, employee, {new: true});
      return res.send(employee);
    } catch (error) {
      res.status(400).send(error);
    }
  } else {
    res.status(404).send({error: 'Сотрудник не найден'});
  }
});

module.exports = router;