const express = require('express');
const User = require('../../models/User');
const multer = require('multer');
const config = require("../../config");
const {nanoid} = require("nanoid");
const path = require('path');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.post('/', upload.single('avatar'), async (req, res) => {
  const preUser = {
    email: req.body.email,
    password: req.body.password,
    name: req.body.name,
    phone: req.body.phone,
    address: req.body.address,
  }

  if (req.file) preUser.avatar = '/uploads/' + req.file.filename;

  const user = new User(preUser);
  try {
    user.generateToken();
    await user.save();
    res.send(user);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.post('/sessions', async (req, res) => {
const errMessage = {
  message: 'Пароль или электронная почта неверны'
}
  try {
    const user = await User.findOne({email: req.body.email});
    if (!user) return res.status(401).send({errors: errMessage});
    const isMatch = await user.checkPassword(req.body.password);
    if (!isMatch) return res.status(401).send({errors: errMessage});
    if (user.status === 'inactive') return res.status(401).send({global: 'Ваш профиль еще неактивирован'});
    user.generateToken();
    await user.save({validateBeforeSave: false});
    res.send(user);
  } catch (err) {
    res.status(500).send({global: 'Internal Server Error'});
  }

});

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  if (!token) return res.status(400).send({global:'Вы не вошли в систему'});
  const user = await User.findOne({token});
  if (!user) return res.status(400).send({global: 'Этот пользователь не существует'});
  try {
    user.generateToken();
    await user.save({validateBeforeSave: false});
    return res.send({global: 'Вы успешно вышли из системы'});
  } catch (err) {
    res.status(500).send({global: 'Internal Server Error'});
  }

});

module.exports = router;