const express = require('express');
const authForEmployee = require('../../middleware/authForEmployee');
const AccountsDetail = require('../../models/AccountsDetail');

const router = express.Router();

router.get('/', authForEmployee, async (req, res) => {
	try {
		const accounts = await AccountsDetail.find();
		res.send(accounts);
	} catch (error) {
		res.sendStatus(500);
	}
});

module.exports = router;
