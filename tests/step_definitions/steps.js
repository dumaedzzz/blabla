const {I} = inject();

Given(`I have entered in {string} page`, (page) => {
  switch (page) {
    case 'login': {
      I.amOnPage(page);
    }
  }
});
When(`I enter my details data :`, (table) => {
  table.rows.forEach(row => {
    I.fillField(row.cells[0].value, row.cells[1].value);
  })
});
When(`press button {string}`, () => {
  I.click('//form//button[@type="submit"]');
});
Then(`I can see text {string}`, (text) => {
  I.see(text);
  I.wait(3)
});
