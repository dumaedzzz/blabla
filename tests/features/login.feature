Feature: Login user
  As a registered user
  I should be able to log into the system
  When I enter my details data

  @UserLogIn
  Scenario:
    Given I have entered in "login" page
    When I enter my details data :
      | email    | user@gmail.com |
      | password | 123            |
    And press button "Войти"
    Then I can see text "Ремонт гидротрансформаторов"